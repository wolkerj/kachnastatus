#!/usr/bin/env bash

#
# KachnaStatus
# Copyright (c) Jiří Wolker 2023
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 version-name"
	exit 1
fi >&2

read -r x y z < <(sed 's/\([0-9]*\)\.\([0-9]*\)\.\([0-9]*\).*/\1 \2 \3/' <<<"$*.0.0")

echo $(("$x" * 100000 + "$y" * 1000 + "$z"))
