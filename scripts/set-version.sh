#! /bin/sh

#
# KachnaStatus
# Copyright (c) Jiří Wolker 2023
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

if [ $# -ne 1 ]; then
	echo "Usage: $0 new_version_number"
	exit 1
fi >&2

new_version="$1"
new_version_code="$(./scripts/version-name-to-code.sh "$new_version")"

sed -i "s/\(versionName\)\s.*/\1 \"$new_version\"/g" ./app/build.gradle
sed -i "s/\(versionCode\)\s.*/\1 $new_version_code/g" ./app/build.gradle
