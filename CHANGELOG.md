Version 0.1.1 (2023-03-05)
==========================

Preparations for publishing on F-Droid.

Version 0.1 (2023-03-05)
========================

This is the first published version of the application. It provides basic
functionality and has really many bugs, non-complete features and many
features are completely missing. The app can currently show you:

* state of the club (closed, chillzone, open, event etc.),
* current event in the club,
* prestige leaderboard,
* currently offered products,
* calendar with upcoming events
