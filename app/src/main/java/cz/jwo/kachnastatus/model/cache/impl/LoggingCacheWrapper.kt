/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import android.util.Log
import cz.jwo.kachnastatus.model.ModelResponse

/**
 * A wrapper over [ModelResponseCache] that logs all accesses to the system [Log].
 */
class LoggingCacheWrapper<T>(
    /**
     * The cache that is used to provide the data.
     */
    private val inner: ModelResponseCache<T>,

    /**
     * A tag value provided to [Log] methods.
     */
    private val tag: String,
) : ModelResponseCache<T>() {
    override suspend fun getEntry(key: String, allowStale: Boolean): ModelResponse<T>? =
        inner.getEntry(key).also {
            Log.d(tag, "Retrieving entry “$key”: ${if (it != null) "hit" else "miss"}")
        }

    override suspend fun deleteEntry(key: String) =
        inner.deleteEntry(key).also {
            Log.d(tag, "Deleting entry “$key”.")
        }

    override suspend fun prune() =
        inner.prune().also {
            Log.d(tag, "Pruning the cache.")
        }

    override suspend fun storeEntry(key: String, value: ModelResponse<T>) =
        inner.storeEntry(key, value).also {
            Log.d(tag, "Updating cache entry for “$key”.")
        }
}

/**
 * Wraps the cache in [LoggingCacheWrapper].
 *
 * @param tag A value passed to [Log] calls.
 */
fun <T> ModelResponseCache<T>.logAccesses(tag: String) =
    LoggingCacheWrapper(this, tag)