/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import cz.jwo.kachnastatus.model.ModelResponse
import java.util.*

/**
 * Returns whether the cache entry is older than some specified time in the past.
 *
 * @param expirationMillis A time specified by number of milliseconds to the past after which all
 * cache entries are considered to be stale.
 */
internal fun <T> ModelResponse<T>.isStaleAfter(expirationMillis: Long) =
    accessed.time + expirationMillis < Date().time

/**
 * Takes the value, but only if the response was not produced at some specified time in the past.
 *
 * @param expirationMillis Offset from the current time to the past at which the response expires.
 */
internal fun <T> ModelResponse<T>.takeUnlessStale(expirationMillis: Long) =
    takeUnless { isStaleAfter(expirationMillis) }