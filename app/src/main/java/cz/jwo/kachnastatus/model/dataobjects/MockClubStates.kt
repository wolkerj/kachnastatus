/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

/**
 * Fictional people that create the [MockClubStates].
 */
private object MockClubStateCreators {
    val mockUserOlo = ClubStateCreator(
        name = "Oldřich Otvíral",
        nickname = "OlO",
        email = "xlogin00@su.fit.vutbr.cz",
    )
    val MockUserWuw = ClubStateCreator(
        name = "Evžen Malý",
        nickname = "uwu",
        email = null,
    )
}

/**
 * Fictional club states (for testing).
 */
object MockClubStates {
    val openWithBar = ClubState(
        type = StateType.OpenBar,
        creator = MockClubStateCreators.mockUserOlo,
        start = Date.from(Instant.now().minus(4, ChronoUnit.HOURS)),
        plannedEnd = Date.from(Instant.now().plus(4, ChronoUnit.HOURS).minus(30, ChronoUnit.MINUTES)),
        note = "Testovací otvíračka",
        eventId = null,
        followingState = null,
        id = 42,
    )
    val openChillzone = ClubState(
        type = StateType.OpenChillzone,
        creator = MockClubStateCreators.MockUserWuw,
        start = Date.from(Instant.now().minus(4, ChronoUnit.HOURS)),
        plannedEnd = null,
        note = "Testovací chillzóna",
        eventId = null,
        followingState = null,
        id = 43,
    )
    val private = ClubState(
        type = StateType.Private,
        creator = MockClubStateCreators.MockUserWuw,
        start = Date.from(Instant.now().minus(4, ChronoUnit.HOURS)),
        plannedEnd = null,
        note = "Dungeons&Dragons",
        eventId = null,
        followingState = null,
        id = 43,
    )
    val closed = ClubState(
        type = StateType.Closed,
        creator = null,
        start = Date.from(Instant.now().minus(4, ChronoUnit.HOURS)),
        plannedEnd = null,
        note = null,
        eventId = null,
        followingState = null,
        id = 0,
    )
}