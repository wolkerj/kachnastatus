/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import kotlinx.serialization.Serializable

/**
 * Information about the currently offered items in the club.
 */
@Serializable
data class CurrentOffer(
    /**
     * List of all products declared as available (by the club staff).
     *
     * This list does not include beers on tap.
     */
    val products: List<OfferedItem>,
    /**
     * List of beers on tap.
     */
    val beersOnTap: List<OfferedItem>,
) {
    /**
     * List of all items
     */
    val allItems by lazy { products + beersOnTap }

    /**
     * Labels used to label the items.
     *
     * The labels are used to categorize the items.
     *
     * @see OfferedItem.labels for details
     */
    val labels by lazy { allItems.flatMap { it.labels }.toSet() }

    /**
     * Offered items categorized by their labels.
     *
     * One item can have multiple labels; In this case, the item is returned multiple times.
     */
    val itemsByLabel by lazy { labels.map { label -> Pair(label, getItemsByLabel(label)) } }

    /**
     * Returns list of items (both products and beers on tap) that have assigned the specified
     * label.
     */
    fun getItemsByLabel(label: String) =
        allItems.filter { item -> item.labels.contains(label) }
}