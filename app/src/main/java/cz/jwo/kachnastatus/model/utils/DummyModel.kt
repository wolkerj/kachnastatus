/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.utils

import cz.jwo.kachnastatus.model.Model
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.*
import java.util.*

/**
 * A [Model] implementation that always fails.
 */
object DummyModel : Model {
    override suspend fun getCurrentClubState(): ModelValueFlow<ClubState> {
        fail()
    }

    override suspend fun getClubState(stateId: Int): ModelValueFlow<ClubState> {
        fail()
    }

    override suspend fun getClubStatesBetween(startDate: Date, endDate: Date): ModelValueFlow<ClubStateList> {
        fail()
    }

    override suspend fun getPrestigeLeaderboardToday(): ModelValueFlow<PrestigeInfo> {
        fail()
    }

    override suspend fun getPrestigeLeaderboardSemester(): ModelValueFlow<PrestigeInfo> {
        fail()
    }

    override suspend fun getEvent(eventId: Int): ModelValueFlow<Event> {
        fail()
    }

    override suspend fun getEventsBetween(startDate: Date, endDate: Date): ModelValueFlow<EventList> {
        fail()
    }

    override suspend fun getCurrentEvents(): ModelValueFlow<EventList> {
        fail()
    }

    override suspend fun getCurrentOffer(): ModelValueFlow<CurrentOffer> {
        fail()
    }

    /**
     * Fail with exception that hopefully notifies the developer that a [DummyModel] is being used.
     */
    private fun fail(): Nothing = throw IllegalStateException("using dummy data model is not possible")
}