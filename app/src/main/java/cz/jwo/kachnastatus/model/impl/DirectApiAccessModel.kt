/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.impl

import cz.jwo.kachnastatus.api.ApiClient
import cz.jwo.kachnastatus.model.Model
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.*
import cz.jwo.kachnastatus.model.valueAsImmutableModelValueFlow
import java.util.*

/**
 * A [Model] that performs all operations directly on the API.
 *
 * This model is usually used in combination with models that provide caching. This model does not
 * perform caching.
 *
 * All methods of this class map directly to the API calls.
 */
class DirectApiAccessModel(private val apiClient: ApiClient) : Model {
    override suspend fun getCurrentClubState(): ModelValueFlow<ClubState> =
        apiClient.getCurrentState().valueAsImmutableModelValueFlow()

    override suspend fun getClubStatesBetween(startDate: Date, endDate: Date): ModelValueFlow<ClubStateList> =
        apiClient.getStates(startDate, endDate).valueAsImmutableModelValueFlow()

    override suspend fun getClubState(stateId: Int): ModelValueFlow<ClubState> =
        apiClient.getClubState(stateId).valueAsImmutableModelValueFlow()

    override suspend fun getPrestigeLeaderboardToday(): ModelValueFlow<PrestigeInfo> =
        apiClient.getPrestigeToday().valueAsImmutableModelValueFlow()

    override suspend fun getPrestigeLeaderboardSemester(): ModelValueFlow<PrestigeInfo> =
        apiClient.getPrestigeSemester().valueAsImmutableModelValueFlow()

    override suspend fun getEvent(eventId: Int): ModelValueFlow<Event> =
        apiClient.getEvent(eventId).valueAsImmutableModelValueFlow()

    override suspend fun getEventsBetween(startDate: Date, endDate: Date): ModelValueFlow<EventList> =
        apiClient.getEvents(startDate, endDate).valueAsImmutableModelValueFlow()

    override suspend fun getCurrentEvents(): ModelValueFlow<EventList> =
        apiClient.getCurrentEvents().valueAsImmutableModelValueFlow()

    override suspend fun getCurrentOffer(): ModelValueFlow<CurrentOffer> =
        apiClient.getCurrentOffer().valueAsImmutableModelValueFlow()
}