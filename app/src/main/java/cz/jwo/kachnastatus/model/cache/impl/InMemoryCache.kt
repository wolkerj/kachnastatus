/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.toModelResponse
import java.util.*

/**
 * A [ModelResponseCache] that stores the cached entries in the memory.
 */
class InMemoryCache<T>(
    /**
     * How much time does it take to expire a cache entry stored in this cache.
     */
    private val expirationMillis: Long,
) : ModelResponseCache<T>() {
    /**
     * The underlying storage of the cache.
     */
    private val entries = mutableMapOf<String, CacheEntry<T>>()

    override suspend fun getEntry(key: String, allowStale: Boolean): ModelResponse<T>? =
        entries[key]?.toModelResponse()?.takeUnless { cachedResponse ->
            (allowStale || cachedResponse.isStaleAfter(expirationMillis))
                .also { stale ->
                    if (stale && !allowStale) {
                        deleteEntry(key)
                    }
                }
        }

    override suspend fun storeEntry(key: String, value: ModelResponse<T>) {
        entries[key] = CacheEntry(
            value = value.value,
            accessed = value.accessed,
            expires = Date(Date().time + expirationMillis),
        )
    }

    override suspend fun deleteEntry(key: String) {
        entries.remove(key)
    }

    override suspend fun prune() {
        val now = Date()
        entries.forEach { (key, entry) ->
            if (entry.expires.after(now)) {
                deleteEntry(key)
            }
        }
    }

    /**
     * Entry of [InMemoryCache].
     */
    private data class CacheEntry<T>(val value: T, val expires: Date, val accessed: Date) {
        /**
         * Wraps the cached value in [ModelResponse].
         */
        fun toModelResponse(): ModelResponse<T> =
            value.toModelResponse(accessed = accessed)
    }
}

