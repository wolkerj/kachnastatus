/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.utils.mock

import cz.jwo.kachnastatus.model.CurrentClubStateProvider
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.valueAsImmutableModelValueFlow

/**
 * A [CurrentClubStateProvider] that always provides the same data.
 */
class FixedCurrentClubStateProvider(private val clubState: ClubState) : CurrentClubStateProvider {
    override suspend fun getCurrentClubState(): ModelValueFlow<ClubState> =
        clubState.valueAsImmutableModelValueFlow()

    override fun toString(): String =
        "${javaClass.canonicalName}($clubState)"
}