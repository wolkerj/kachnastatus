/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.impl

import cz.jwo.kachnastatus.model.*
import cz.jwo.kachnastatus.model.cache.impl.ModelResponseCache
import cz.jwo.kachnastatus.model.cache.impl.getEntryOrProvide
import cz.jwo.kachnastatus.model.dataobjects.ClubStateList
import cz.jwo.kachnastatus.model.dataobjects.EventList
import cz.jwo.kachnastatus.model.utils.CompositeModel
import cz.jwo.kachnastatus.util.*
import kotlinx.coroutines.*
import kotlinx.serialization.Serializable
import java.util.*

/**
 * Returns cache key for an event or club state.
 */
private fun cacheKeyForItem(eventId: Int, queryType: QueryType) =
    "$queryType-$eventId"

/**
 * Returns cache key for list of events or club states that happen during some specified day.
 *
 * @param dayStart Day start timestamp (millis since epoch)
 */
private fun cacheKeyForDayItems(dayStart: Long, queryType: QueryType) =
    "day-$dayStart-$queryType"
        .also { require(dayStart % (24 * 3600_000) == 0.toLong()) { "tried to use non-midnight for list of day events" } }

/**
 * Returns cache key for list of events or club states that happen during some specified day.
 */
private fun cacheKeyForDayItems(dayStart: Date, queryType: QueryType) =
    cacheKeyForDayItems(dayStart.time, queryType)

/**
 * Uses cache for requests to events and club states.
 */
class EventAndClubStateCacheHook(
    /**
     * The underlying cache.
     */
    private val cache: ModelResponseCache<CacheEntry>,

    /**
     * Event provider used to fetch event information on cache misses.
     */
    private val innerEventProvider: EventProvider,
    /**
     * Club state provider used to fetch club state information on cache misses.
     */
    private val innerClubStateProvider: ClubStateProvider,
) : EventProvider, ClubStateProvider {
    override suspend fun getClubStatesBetween(startDate: Date, endDate: Date): ModelValueFlow<ClubStateList> =
        getItemsBetween(QueryType.ClubStates, startDate, endDate)
            .mapStateFlow {
                it.map { dayItems ->
                    dayItems.map { item ->
                        (item as DayItem.ClubState).clubState
                    }
                }
            }

    override suspend fun getEventsBetween(startDate: Date, endDate: Date): ModelValueFlow<EventList> =
        getItemsBetween(QueryType.Events, startDate, endDate)
            .mapStateFlow {
                it.map { dayItems ->
                    dayItems.map { item ->
                        (item as DayItem.Event).event.event
                    }
                }
            }

    override suspend fun getClubState(stateId: Int): ModelValueFlow<cz.jwo.kachnastatus.model.dataobjects.ClubState> =
        cache.getEntryOrProvide(cacheKeyForItem(stateId, QueryType.ClubStates)) {
            innerClubStateProvider.getClubState(stateId)
                .mapStateFlow { modelResponse -> modelResponse.map { CacheEntry.ClubState(it) } }
        }
            .mapStateFlow { modelResponse -> modelResponse.map { (it as CacheEntry.ClubState).clubState } }

    override suspend fun getEvent(eventId: Int): ModelValueFlow<cz.jwo.kachnastatus.model.dataobjects.Event> =
        cache.getEntryOrProvide(cacheKeyForItem(eventId, QueryType.Events)) {
            innerEventProvider.getEvent(eventId)
                .mapStateFlow { modelResponse -> modelResponse.map { CacheEntry.Event(it) } }
        }
            .mapStateFlow { modelResponse -> modelResponse.map { (it as CacheEntry.Event).event } }

    /**
     * Returns club states or events in the specified time range.
     *
     * @param queryType Which item kind to return
     */
    private suspend fun getItemsBetween(
        queryType: QueryType,
        startDate: Date,
        endDate: Date,
    ): ModelValueFlow<List<DayItem>> {
        // We extend the range to include whole days.
        val startDateMidnight = startDate.midnight
        val endDateMidnight = endDate.midnightAfter

        // Try to get events for the days from the cache.
        val days = midnightsBetween(startDateMidnight, endDateMidnight)
            .map { dayStart -> DateRange(dayStart, dayStart.midnightAfter) }
        return coroutineScope {
            try {
                // Fetch event ID lists from the cache for each of the days.
                awaitAll(*days.map { (dayStart, _) ->
                    async {
                        cache.getEntry(cacheKeyForDayItems(dayStart, queryType))
                            ?: throw CancellationException()
                    }
                }.toTypedArray())
                    // Unwrap the event ID list from the cache entry.
                    .map { it.map { cacheEntry -> (cacheEntry as CacheEntry.DayItems).items } }
                    // Merge the event ID lists to one ModelResponse<List<Int>>.
                    .reduce { acc, b -> acc.map { it + b.value } }
                    // Get event data for each of the event IDs (or keep if we are getting club states).
                    .map { // (ModelResponse::map)
                        it.map { it -> // (List::map)
                            if (it is DayItem.EventId) {
                                cache.getEntry(
                                    cacheKeyForItem(
                                        it.eventId,
                                        queryType
                                    )
                                )
                                    ?.let { event -> DayItem.Event(event.value as CacheEntry.Event) }
                                    ?: throw CancellationException()

                                // When the entry is not found, everything is discarded: we have a cache miss.

                            } else {
                                it
                            }
                        }
                    }
                    // Make the value edible by the caller.
                    .asImmutableModelValueFlow()
            } catch (_: CancellationException) {
                // Cache miss: Get the events or club states,
                when (queryType) {
                    QueryType.Events ->
                        fetchAndCacheEventsBetween(startDateMidnight, endDateMidnight, days)
                            .mapStateFlow {
                                it.map {
                                    it.map { DayItem.Event(CacheEntry.Event(it)) }
                                }
                            }

                    QueryType.ClubStates ->
                        fetchAndCacheClubStates(startDateMidnight, endDateMidnight, days)
                            .mapStateFlow {
                                it.map { clubStateList ->
                                    clubStateList.map { clubState -> DayItem.ClubState(clubState) }
                                }
                            }
                }
            }
        }
    }

    /**
     * Fetches and stores club state information.
     */
    private suspend fun fetchAndCacheClubStates(
        startDateMidnight: Date,
        endDateMidnight: Date,
        days: List<DateRange>,
    ) =
        innerClubStateProvider.getClubStatesBetween(startDateMidnight, endDateMidnight)
            .also { result ->
                val response = result.value
                val clubStates = response.value
                coroutineScope {
                    awaitAll(*days.map { dayRange ->
                        val (dayStart, _) = dayRange
                        async {
                            cache.storeEntry(
                                cacheKeyForDayItems(dayStart, QueryType.ClubStates),
                                CacheEntry.DayItems(clubStates
                                    .filter { it.from.between(dayRange) || (it.to?.between(dayRange) ?: false) }
                                    .map(DayItem::ClubState))
                                    .asModelResponse(response.accessed)
                            )
                        }
                    }.toTypedArray())
                }
            }

    /**
     * Fetches and stores event information.
     */
    private suspend fun fetchAndCacheEventsBetween(
        startDateMidnight: Date,
        endDateMidnight: Date,
        days: List<DateRange>,
    ) = innerEventProvider.getEventsBetween(startDateMidnight, endDateMidnight)
        .also { result ->
            val response = result.value
            val events = response.value
            coroutineScope {
                val toAwait = mutableListOf<Deferred<*>>()

                // store the event data in cache entries,
                toAwait += events.map { event ->
                    async {
                        cache.storeEntry(
                            key = cacheKeyForItem(eventId = event.id, QueryType.Events),
                            value = CacheEntry.Event(event)
                                .asModelResponse(response.accessed)
                        )
                    }
                }
                // and also store event lists for each of the days.
                toAwait += days.map { dayRange ->
                    events
                        .filter { event -> event.from.between(dayRange) || event.to.between(dayRange) }
                        .let { eventsOfDay ->
                            async {
                                cache.storeEntry(
                                    key = cacheKeyForDayItems(dayRange.from, QueryType.Events),
                                    value = CacheEntry.DayItems(
                                        eventsOfDay.map { DayItem.EventId(it.id) }
                                    ).asModelResponse(response.accessed)
                                )
                            }
                        }
                }

                // Perform the deferred operations.
                //awaitAll(*toAwait.toTypedArray())
                toAwait.forEach { it.await() }
            }
        }

    /**
     * Cache entry type for [EventAndClubStateCacheHook].
     */
    @Serializable
    sealed class CacheEntry {
        @Serializable
        internal class Event(val event: cz.jwo.kachnastatus.model.dataobjects.Event) : CacheEntry()

        @Serializable
        internal class ClubState(val clubState: cz.jwo.kachnastatus.model.dataobjects.ClubState) : CacheEntry()

        @Serializable
        internal class DayItems(val items: List<DayItem>) : CacheEntry()
    }

    /**
     * Item stored in [CacheEntry.DayItems].
     */
    @Serializable
    internal sealed class DayItem {
        /**
         * Event ID.
         *
         * This subclass of [DayItem] is used for storage on the disk.
         */
        @Serializable
        class EventId(val eventId: Int) : DayItem()

        /**
         * Event.
         *
         * This subclass of [DayItem] is used **at runtime only**. To store events on the disk, use
         * [EventId].
         */
        @Serializable
        class Event(val event: CacheEntry.Event) : DayItem()

        /**
         * Club state.
         */
        @Serializable
        class ClubState(val clubState: cz.jwo.kachnastatus.model.dataobjects.ClubState) : DayItem()
    }
}

/**
 * Type of (cache, database) query performed by [CurrentClubStateCacheHook].
 */
private enum class QueryType {
    Events, ClubStates;

    override fun toString(): String = when (this) {
        Events -> "event"
        ClubStates -> "state"
    }
}

/**
 * Returns an instance of [EventAndClubStateCacheHook].
 *
 * See [EventAndClubStateCacheHook] constructor for details.
 */
fun EventAndClubStateCacheHook(cache: ModelResponseCache<EventAndClubStateCacheHook.CacheEntry>): CompositeModel.(Model) -> Unit =
    { model ->
        val hook = EventAndClubStateCacheHook(cache, model, model)
        eventProvider = hook
        clubStateProvider = hook
    }