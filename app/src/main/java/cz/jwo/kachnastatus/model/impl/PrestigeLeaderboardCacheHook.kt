/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.impl

import cz.jwo.kachnastatus.model.Model
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.PrestigeLeaderboardProvider
import cz.jwo.kachnastatus.model.cache.impl.ModelResponseCache
import cz.jwo.kachnastatus.model.cache.impl.getEntryOrProvide
import cz.jwo.kachnastatus.model.dataobjects.PrestigeInfo
import cz.jwo.kachnastatus.model.utils.CompositeModel
import cz.jwo.kachnastatus.util.midnight
import java.util.*

/**
 * A cache hook that caches leaderboard items.
 */
class PrestigeLeaderboardCacheHook(
    /**
     * The cache used to store the values.
     */
    private val cache: ModelResponseCache<PrestigeInfo>,

    /**
     * The model wrapped by the caching hook.
     */
    private val innerModel: PrestigeLeaderboardProvider,
) : PrestigeLeaderboardProvider {
    override suspend fun getPrestigeLeaderboardToday(): ModelValueFlow<PrestigeInfo> =
        cache.getEntryOrProvide(Date().midnight.time.toString()) {
            innerModel.getPrestigeLeaderboardToday()
        }

    override suspend fun getPrestigeLeaderboardSemester(): ModelValueFlow<PrestigeInfo> =
        cache.getEntryOrProvide("semester") {
            innerModel.getPrestigeLeaderboardSemester()
        }
}

/**
 * Instantiates [PrestigeLeaderboardCacheHook].
 *
 * See [PrestigeLeaderboardCacheHook] constructor for more information.
 */
fun PrestigeLeaderboardCacheHook(cache: ModelResponseCache<PrestigeInfo>): CompositeModel.(Model) -> Unit = { model ->
    prestigeLeaderboardProvider = PrestigeLeaderboardCacheHook(cache, model)
}