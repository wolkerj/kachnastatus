/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model

import cz.jwo.kachnastatus.util.TimestampSerializer
import kotlinx.serialization.Serializable
import java.util.*

/**
 * Response from the API with metadata.
 */
@Serializable
data class ModelResponse<T>(
    /**
     * Value returned by the API.
     */
    val value: T,

    /**
     * Access time to the resource.
     */
    @Serializable(TimestampSerializer::class)
    val accessed: Date,
) {
    /**
     * Modifies the [ModelResponse] [value] using the given function.
     *
     * This can be useful for conversion of the data representation between different types.
     */
    inline fun <reified R> map(transform: (T) -> R) =
        ModelResponse(transform(value), accessed)
}

/**
 * Wraps the values in a [ModelResponse].
 *
 * @param accessed value access time
 */
fun <T> T.toModelResponse(accessed: Date) =
    ModelResponse(value = this, accessed = accessed)