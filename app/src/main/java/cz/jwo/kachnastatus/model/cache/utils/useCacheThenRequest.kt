/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.utils

import android.os.Parcelable
import android.util.Log
import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.ModelValueFlow
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.stateIn

/**
 * Performs a network request and fetches the data from the cache, providing the first available.
 *
 * This function starts both network request and cache access in parallel and returns
 * an instance of [ModelValueFlow]. The flow emits the value from the cache (if the cache contained
 * the value) and then value retrieved from the server. When the server responds faster than the
 * cache, the cached value is not emitted.
 */
suspend inline fun <reified T : Parcelable> useCacheAndRequest(
    crossinline getCached: suspend () -> ModelValueFlow<T>?,
    crossinline fetchCurrent: suspend () -> ModelValueFlow<T>,
    stateCoroutineScope: CoroutineScope,
): ModelValueFlow<T> =
    coroutineScope {
        val cached = async { getCached() }
        val current = async { fetchCurrent() }
        current.invokeOnCompletion { cached.cancel() }
        merge<ModelResponse<T>>(
            flow {
                try {
                    cached.await()?.let { emit(it.first()) }
                } catch (_: CancellationException) {
                    Log.d(
                        "useCacheThenRequest",
                        "Some request was completed before its result was read from the cache."
                    )
                }
            },
            flow { emit(current.await().first()) },
        ).stateIn(stateCoroutineScope)
    }