/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.*

/**
 * Stream of values from the model.
 *
 * The first value should be immediately available for consumption. When the model detects state
 * change, it can emit new value in this flow.
 */
typealias ModelValueFlow<T> = StateFlow<ModelResponse<T>>

/**
 * Wraps an immutable value in [ModelValueFlow].
 *
 * The returned [ModelValueFlow] will never emit further values.
 */
inline fun <reified T> valueAsImmutableModelValueFlow(value: T, fetchDate: Date = Date()) =
    MutableStateFlow(ModelResponse(value, fetchDate))

/**
 * Wraps an immutable value in [ModelValueFlow].
 *
 * The returned [ModelValueFlow] will never emit further values.
 */
inline fun <reified T> T.valueAsImmutableModelValueFlow(fetchDate: Date = Date()): ModelValueFlow<T> =
    valueAsImmutableModelValueFlow(this, fetchDate)

/**
 * Wraps an [ModelResponse] in [ModelValueFlow].
 *
 * The returned [ModelValueFlow] will never emit further values.
 */
inline fun <reified T> ModelResponse<T>.asImmutableModelValueFlow(): ModelValueFlow<T> =
    MutableStateFlow(this)

/**
 * Wraps an [ModelResponse] in [ModelValueFlow].
 *
 * The returned [ModelValueFlow] will never emit further values.
 */
inline fun <reified T> T.asModelResponse(fetchDate: Date = Date()) =
    ModelResponse(this, fetchDate)