/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import cz.jwo.kachnastatus.model.ModelResponse
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope

/**
 * Delegates cache accesses to multiple caches.
 *
 * This class can be used to combine both in-memory and disk caches.
 */
class CacheCombinator<T>(
    /**
     * List of caches used to access the entries.
     *
     * The caches are ordered by their priority: the first cache is checked first.
     */
    private val caches: List<ModelResponseCache<T>>,
) : ModelResponseCache<T>() {
    constructor(
        /**
         * List of caches used to access the entries.
         *
         * The caches are ordered by their priority: the first cache is checked first.
         */
        vararg caches: ModelResponseCache<T>,
    )
            : this(caches.toList())

    /**
     * Returns entry from the first cache containing it.
     *
     * The value will be pushed to all caches that have higher priority than the one from which
     * the value was fetched.
     */
    override suspend fun getEntry(key: String, allowStale: Boolean): ModelResponse<T>? =
        cacheIterationOrder
            // Get the entry from the first cache that hits.
            .firstNotNullOfOrNull { (cache, cachesToUpdate) ->
                cache.getEntry(key, allowStale)
                    ?.also { value -> // Store the entry in all caches above this one.
                        forAllCaches(cachesToUpdate) { cache ->
                            cache.storeEntry(key, value)
                        }
                    }
            }

    /**
     * Order of iteration over caches.
     *
     * This is a list of pairs. The **first** component of the pair is the cache that is used for
     * fetching the values. The **second** component is a list of cached that must be updated when
     * a value is fetched from the given cache.
     */
    private val cacheIterationOrder = caches.runningFold(
        Pair(
            caches.first(),
            emptyList<ModelResponseCache<T>>()
        )
    ) { (cache, cachesToUpdate), nextCache ->
        Pair(nextCache, cachesToUpdate + listOf(cache))
    }

    /**
     * Stores entry in all the caches.
     */
    override suspend fun storeEntry(key: String, value: ModelResponse<T>) {
        forAllCaches {
            it.storeEntry(key, value)
        }
    }

    /**
     * Removes all entries with the given [key] from all the caches.
     */
    override suspend fun deleteEntry(key: String) {
        forAllCaches {
            it.deleteEntry(key)
        }
    }

    /**
     * Prunes all the underlying caches.
     */
    override suspend fun prune() {
        forAllCaches {
            it.prune()
        }
    }

    /**
     * Performs some operation for all caches, in parallel.
     */
    private suspend fun forAllCaches(
        caches: Iterable<ModelResponseCache<T>> = this.caches,
        block: suspend (ModelResponseCache<T>) -> Unit,
    ) {
        coroutineScope {
            awaitAll(*caches.distinct().map { cache ->
                async { block(cache) }
            }.toTypedArray())
        }
    }
}