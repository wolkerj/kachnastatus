/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model

import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.*
import java.util.*

/**
 * Returns all club states and events in one list.
 *
 * This method fetches the both lists asynchronously.
 *
 * The parameter [statesEndDate] is provided to allow fetching different ranges of states
 * and events. The API can have different limits on these two fetches.
 *
 * Events and states that happen only partially in the specified range are not returned.
 *
 * @param startDate Start of the range to fetch.
 * @param endDate End of the range to fetch.
 * @param statesEndDate Overrides [endDate] when fetching the club states.
 * @param stateFlowScope Coroutine scope to use for [StateFlow] creation.
 */
suspend fun Model.getEventsAndStatesBetween(
    startDate: Date,
    endDate: Date,
    statesEndDate: Date = endDate,
    stateFlowScope: CoroutineScope,
): ModelValueFlow<List<EventOrState>> = with(stateFlowScope) {
    // Get the events and states as multiple flows, each producing values asynchronously.
    kotlinx.coroutines.coroutineScope {
        awaitAll(
            async { getClubStatesBetween(startDate, statesEndDate) },
            async { getEventsBetween(startDate, endDate) },
        )
    }
        // Insert index of the flow to each of the returned values.
        .mapIndexed { index, flow -> flow.map { IndexedValue(index, it) } }
        // Merge the flows.
        .let { merge(*it.toMutableList().toTypedArray()) }
        .runningFold(List<ModelResponse<out List<EventOrState>>?>(2) { null }) { acc, (index, response) ->
            acc.mapIndexed { i, oldValue ->
                if (i == index) response
                else oldValue
            }
        }
        .map { it.filterNotNull() }
        // We maintain the time in ModelResponse equal to time of the last response returned.
        .mapNotNull { responses ->
            val lastResponseTime = responses.maxOfOrNull { it.accessed } ?: return@mapNotNull null
            ModelResponse(
                value = responses.map { it.value }.flatten(),
                accessed = lastResponseTime,
            )
        }
        // Make a StateFlow from it.
        .stateIn(this)
}