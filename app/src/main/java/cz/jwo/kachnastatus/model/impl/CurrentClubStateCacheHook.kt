/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.impl

import cz.jwo.kachnastatus.model.CurrentClubStateProvider
import cz.jwo.kachnastatus.model.Model
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.cache.impl.ModelResponseCache
import cz.jwo.kachnastatus.model.cache.impl.getEntryOrProvide
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.utils.CompositeModel

private const val DEFAULT_KEY = "_current"

/**
 * Uses caching on requests to the current club state.
 */
class CurrentClubStateCacheHook(
    private val cache: ModelResponseCache<ClubState>,
    private val innerModel: CurrentClubStateProvider,
    private val key: String = DEFAULT_KEY,
) : CurrentClubStateProvider {
    override suspend fun getCurrentClubState(): ModelValueFlow<ClubState> =
        cache.getEntryOrProvide(key) { innerModel.getCurrentClubState() }
}

/**
 * Uses caching on requests to the current club state.
 */
fun CurrentClubStateCacheHook(
    cache: ModelResponseCache<ClubState>,
    key: String = DEFAULT_KEY,
): CompositeModel.(Model) -> Unit = {
    currentClubStateProvider = CurrentClubStateCacheHook(cache, it, key)
}