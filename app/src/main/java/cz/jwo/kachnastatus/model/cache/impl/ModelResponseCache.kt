/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.asImmutableModelValueFlow

/**
 * A cache that can store [ModelResponse]s.
 *
 * The cache is a simple key-value store. The key type is [String].
 *
 * The [ModelResponseCache] must guarantee that the entry has the proper type.
 *
 * Entries that were cached for a time so long that it can be expected that the actual value has
 * changed are called *stale entries*. Each implementation can define meaning of that precisely.
 */
abstract class ModelResponseCache<T> {
    /**
     * Returns an entry from the cache.
     *
     * Reading from the cache must not change it.
     *
     * @param key Unique identifier of the cache entry
     * @param allowStale Whether to allow returning entries which are stale.
     */
    abstract suspend fun getEntry(key: String, allowStale: Boolean = false): ModelResponse<T>?

    /**
     * Stores a value in the cache, overwriting the previous one.
     */
    abstract suspend fun storeEntry(key: String, value: ModelResponse<T>)

    /**
     * Deletes an entry from the cache.
     */
    abstract suspend fun deleteEntry(key: String)

    /**
     * Removes all stale entries from the cache.
     */
    abstract suspend fun prune()

}

/**
 * Returns a cache entry, fetching the data into it if it was not cached.
 */
suspend inline fun <reified T> ModelResponseCache<T>.getEntryOrProvide(
    key: String,
    provide: (String) -> ModelValueFlow<T>,
): ModelValueFlow<T> =
    getEntry(key)?.asImmutableModelValueFlow() ?: provide(key).also { response ->
        storeEntry(key, response.value)
    }