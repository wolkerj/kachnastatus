/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model

/**
 * Model for the application data.
 *
 * Instances of this class provide methods to perform accesses to the Kachna Online API, optionally
 * with caching and other features.
 *
 * The [Model] is designed to be easy to modify by composition:
 *
 *   * The [Model] is a class that implements all possible `Provider` interfaces. Every provider
 *     specializes only to one aspect of the API: fetching state information fetching events etc.
 *   * To compose a [Model] from multiple different providers,
 *     [cz.jwo.kachnastatus.model.utils.CompositeModel] can be used. The providers can be completely
 *     different classes.
 *   * The model is frequently stored as an immutable value. To facilitate changing the model on the
 *     fly, [cz.jwo.kachnastatus.model.utils.MutableModelWrapper] can be user.
 *
 * Providers that wrap another provider of the same (or similar) type are called *hooks*.
 */
interface Model : CurrentClubStateProvider, ClubStateProvider, PrestigeLeaderboardProvider,
    EventProvider, CurrentEventListProvider, CurrentOfferProvider