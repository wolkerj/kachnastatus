/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.utils

import cz.jwo.kachnastatus.model.Model

/**
 * A [Model] that wraps another [Model] and allows replacing it.
 *
 * This can be used where you can't change the model (e.g. because you called a suspending function
 * and passed the model as an argument), but you need to change its behavior (e.g. change caching
 * settings).
 */
class MutableModelWrapper private constructor(private val compositeModel: CompositeModel) :
    Model by CompositeModel(compositeModel) {

    constructor(model: Model) : this(CompositeModel(model))

    /**
     * Sets the inner model.
     *
     * Use property [innerModel] instead.
     */
    private fun setModel(model: Model) {
        compositeModel.setAllComponents(model)
    }

    override fun toString(): String =
        "${super.toString()}($innerModel)"

    /**
     * Add a hook to the model.
     *
     * This method provides an elegant way of modifying behavior of the [innerModel]: The function
     * [hook] is called with the original model as an argument and new [CompositeModel] (that uses
     * the original model to perform all operations) as a receiver. The function is expected to
     * perform any needed modifications on the [CompositeModel]. The new [CompositeModel] becomes
     * the new [innerModel].
     */
    fun hook(hook: CompositeModel.(Model) -> Unit) {
        innerModel = compositeModel.copy().apply { hook.invoke(this, innerModel) }
    }

    /**
     * The model that is wrapped by this [MutableModelWrapper].
     *
     * This property also allows changing the model.
     */
    var innerModel: Model
        get() =
            // We just get any of the model instances (they are all the same).
            compositeModel.currentClubStateProvider as Model
        set(it) {
            setModel(it)
        }
}