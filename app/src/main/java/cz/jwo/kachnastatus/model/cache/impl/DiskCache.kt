/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.cache.impl

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import cz.jwo.kachnastatus.BuildConfig
import cz.jwo.kachnastatus.model.ModelResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.serializer
import java.io.File
import java.util.*

private const val TAG = "DiskCache"

/**
 * Cache that serializes all cached values and stores them on the disk.
 *
 * The underlying storage is implemented as a SQLite database. Multiple instances of [DiskCache] can
 * share one database file, given that different [kind]s are set to them and all come from a single
 * version of the application. Given that the [kind]s are different, multiple object types can be
 * stored safely.
 *
 * The [kind] can contain only these characters: `a-z` and a hyphen
 */
class DiskCache<T : Any>(
    /** The underlying database store. */
    private val database: SQLiteDatabase,
    /**
     * A key of the disk cache.
     *
     * Multiple instances of [DiskCache] can share one database to avoid having multiple database
     * connection opened at one time. To separate them, each must be given different [kind]. When
     * the [kind]s are different, it is safe to store different type of values in the cache.
     */
    private val kind: String,
    /**
     * Class of the stored values.
     *
     * Use function `DiskCache` to instantiate this class without having to specify this constructor
     * argument.
     */
    valueClass: Class<T>,
    /**
     * Maximum age of cache entries, in milliseconds.
     */
    private val expirationMillis: Long,
) : ModelResponseCache<T>() {
    override suspend fun getEntry(key: String, allowStale: Boolean): ModelResponse<T>? {
        return withContext(Dispatchers.IO) {
            lateinit var cursor: Cursor
            try {
                cursor = database.query(
                    CacheTable,
                    arrayOf(ValueColumn),
                    "$KeyColumn = ?",
                    arrayOf(packKindKey(kind, key)),
                    null,
                    null,
                    null,
                    null
                )
                if (cursor.moveToFirst()) {
                    @Suppress("UNCHECKED_CAST") // We believe our database.
                    (valueFromBlob(cursor.getBlob(0)) as ModelResponse<T>)
                        .takeUnlessStale(expirationMillis)
                } else null
            } finally {
                cursor.close()
            }
        }
    }

    override suspend fun storeEntry(key: String, value: ModelResponse<T>) {
        withContext(Dispatchers.IO) {
            val expires = Date().time + expirationMillis

            database.insertWithOnConflict(
                CacheTable,
                null,
                ContentValues(1).apply {
                    put(KeyColumn, packKindKey(kind, key))
                    put(ExpiresColumn, expires)
                    put(ValueColumn, valueToBlob(value))
                },
                SQLiteDatabase.CONFLICT_REPLACE
            )
        }
    }

    override suspend fun deleteEntry(key: String) {
        withContext(Dispatchers.IO) {
            database.delete(CacheTable, "$KeyColumn = ?", arrayOf(packKindKey(kind, key)))
        }
    }

    override suspend fun prune() {
        withContext(Dispatchers.IO) {
            database.delete(CacheTable, "$ExpiresColumn > ?", arrayOf(Date().time.toString()))
            database.execSQL("vacuum")
        }
    }

    /**
     * Serializer used to prepare the [ModelResponse]s for storing them on the disk.
     */
    private val valueSerializer = serializer(valueClass)

    /**
     * Deserializes a [ModelResponse] from the on-disk format.
     */
    @Suppress("UNCHECKED_CAST") // We believe our DB.
    private inline fun valueFromBlob(blob: ByteArray): ModelResponse<*> =
        Serializer.decodeFromString(valueSerializer, blob.decodeToString()) as ModelResponse<*>

    /**
     * Serializes the [ModelResponse] to the on-disk format.
     */
    private inline fun <reified V> valueToBlob(value: V): ByteArray =
        Serializer.encodeToString(valueSerializer, value as ModelResponse<*>).encodeToByteArray()

    /**
     * Utilities used to create instance of [DiskCache] and its database.
     */
    companion object DatabaseCreator {
        private const val CacheTable = "cache"

        private const val KeyColumn = "entry_key"
        private const val ValueColumn = "value"
        private const val ExpiresColumn = "expires" // millis since the Unix epoch

        /**
         * Serializer used to store the values.
         */
        private val Serializer = Json {
            serializersModule = SerializersModule {
//                polymorphic(ModelResponse::class)
            }
        }

        /**
         * Packs the kind and key into single value.
         */
        private fun packKindKey(kind: String, key: String): String =
            "$kind:$key"

        /**
         * Creates a database schema for a [DiskCache].
         */
        private fun createSchema(database: SQLiteDatabase) {
            database.execSQL(
                "create table if not exists $CacheTable " +
                        "(" +
                        "$KeyColumn text primary key," +
                        "$ValueColumn blob not null," +
                        "$ExpiresColumn integer not null" +
                        ")"
            )
            database.execSQL(
                "create table if not exists VER (version int)"
            )
        }

        /**
         * Opens a or creates a database and prepares its schema.
         *
         * Application version is not checked.
         */
        private fun openDatabaseWithoutChecks(context: Context): SQLiteDatabase {
            return SQLiteDatabase.openOrCreateDatabase(getDatabaseFile(context), null)
                .also {
                    createSchema(it)
                }
        }

        /**
         * Opens the cache database.
         */
        fun openDatabase(context: Context): SQLiteDatabase {
            Log.d(TAG, "Opening database file.")

            // Check version number in the database. If it is different, delete the database.
            val origVersion = openDatabaseWithoutChecks(context).let { database ->
                getVersionFromDatabase(database)
                    .also { database.close() }
            }
            val runningVersion = runningVersion
            Log.d(TAG, "  - currently running app version: $runningVersion")
            Log.d(TAG, "  - version that created the DB:   ${origVersion?.toString() ?: "<new file>"}")

            if (origVersion != null && origVersion != runningVersion) {
                Log.d(TAG, "Deleting the database file.")
                getDatabaseFile(context).delete()
            }

            Log.d(TAG, "Setting up the database.")
            val db = openDatabaseWithoutChecks(context)
            storeVersion(db)

            return db
        }

        /**
         * Returns application version that was used to create the database file.
         */
        private fun getVersionFromDatabase(database: SQLiteDatabase): Int? {
            return database.query("VER", arrayOf("version"), null, null, null, null, null).let { cursor ->
                (if (cursor.moveToFirst()) cursor.getInt(0) else null)
                    .also {
                        cursor.close()
                    }
            }
        }

        /**
         * Version of the application which is currently running.
         */
        private const val runningVersion = BuildConfig.VERSION_CODE

        /**
         * Stores the current version information in the database.
         *
         * @see getVersionFromDatabase
         */
        private fun storeVersion(database: SQLiteDatabase) {
            database.execSQL("insert into VER (version) values ($runningVersion)")
        }

        /**
         * Returns path to the database file.
         */
        private fun getDatabaseFile(context: Context) =
            File(context.cacheDir, "response_cache.${BuildConfig.VERSION_CODE}.sqlite")
    }
}

/**
 * Creates an instance of [DiskCache].
 *
 * @see DiskCache and its default constructor for details.
 */
inline fun <reified T : Any> DiskCache(
    database: SQLiteDatabase, kind: String, expirationMillis: Long,
) = DiskCache(
    database, kind, T::class.java, expirationMillis
)