/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import cz.jwo.kachnastatus.ui.util.letIf
import cz.jwo.kachnastatus.util.ApiDateSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

/**
 * Information about a club state.
 *
 * This data class provides club state information fetched from the API.
 *
 * When you want to show both events and club states in the UI, you can make use of the superclass
 * [EventOrState].
 *
 * When the club is closed, the current state bah type of [StateType.Closed].
 */
@Serializable
data class ClubState(
    /** Type of the state. */
    @SerialName("state")
    val type: StateType,
    /** Information about the user who made the state. */
    @SerialName("madeByUser")
    val creator: ClubStateCreator?,
    /** Date of the start of the state. */
    @Serializable(ApiDateSerializer::class) val start: Date,
    /** Planned end date of the state. */
    @Serializable(ApiDateSerializer::class) val plannedEnd: Date? = null,
    /** Actual end date of the state. */
    @Serializable(ApiDateSerializer::class) val actualEnd: Date? = null,
    /** Publicly visible note for the state. */
    val note: String?,
    /**
     * ID of the event that is associated with this state or `null`.
     *
     * When the state is associated with an event (event has open bar or something like that), this
     * value is set to the event ID. Additional API call must be made to get event details.
     */
    val eventId: Int?,
    /**
     * Information about the following state.
     *
     * This value is optional and not returned by all API calls. You can expect that it is provided
     * by the “current state” call, but do not rely on it.
     *
     * When this state is [StateType.Closed], this should contain information about the next planned
     * state.
     *
     * Note that this can contain even private event information.
     */
    val followingState: ClubState?,
    /** ID if the club state. */
    val id: Int,
) : EventOrState() {
    /**
     * Returns whether the club is closed at the time of the state.
     *
     * Private events are treated as if the club would be open.
     */
    val isClosed: Boolean
        get() = type == StateType.Closed

    /**
     * Returns whether the club is open at the time of the state.
     *
     * Private events are treated as if the club would be open.
     */
    val isOpen: Boolean
        get() = !isClosed

    /**
     * Constructs a copy of this object with [followingState] field set to the given value.
     */
    fun withFollowingState(followingState: ClubState?) = copy(
        plannedEnd =
        if (this.plannedEnd == null || (followingState?.start != null && this.plannedEnd.after(followingState.start))
        ) {
            followingState?.start
        } else {
            this.plannedEnd
        },
        followingState = followingState,
    )

    /**
     * Constructs a copy of this object that is made into a [StateType.Closed] state if this state
     * is [StateType.Private].
     *
     * This can be used when you want to hide private states from the client.
     */
    fun toClosedIfPrivate(): ClubState =
        this.letIf(type == StateType.Private) {
            copy(type = StateType.Closed)
        }

    /**
     * Short description property for [EventOrState] implementation.
     *
     * Prefer using [note] when you know that the type of the expression is [ClubState].
     */
    override val shortDescription: String?
        get() = note

    /**
     * Start time property for [EventOrState] implementation.
     *
     * Prefer using [start] when you know that the type of the expression is [ClubState].
     */
    override val from: Date
        get() = start

    /**
     * End time property for [EventOrState] implementation.
     *
     * Prefer using [plannedEnd] and/or [actualEnd] when you know that the type of the expression
     * is [ClubState].
     */
    override val to: Date?
        get() = actualEnd ?: plannedEnd
}

/**
 * Type of club state.
 *
 * This is enumeration of all possible states in which the club can (officially and publicly)
 * operate.
 */
@Serializable
enum class StateType {
    OpenBar, OpenChillzone, Private, Closed,
}

/**
 * Information about the person who has created the club state.
 */
@Serializable
data class ClubStateCreator(
    /**
     * Personal name of the creator.
     */
    val name: String,
    /**
     * Nickname of the creator, if any.
     */
    val nickname: String?,
    /**
     * Contact e-mail address of the creator, if any.
     */
    val email: String?,
) {
    /**
     * Complete name of the creator (including its nickname).
     */
    val completeName: String
        get() = if (nickname != null) {
            "$nickname ($name)"
        } else {
            name
        }
}

/**
 * Simple a list of [ClubState]s.
 *
 * Used to simplify signatures of API data processing methods.
 */
typealias ClubStateList = List<ClubState>