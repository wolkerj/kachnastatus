/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import kotlinx.serialization.Serializable

/**
 * An item offered in the club.
 */
@Serializable
data class OfferedItem(
    /**
     * Name (label) of the item.
     *
     * Note that the labels often contain some additional metadata for consumption by the users. It
     * would be nice to filter them out.
     */
    val name: String,
    /** Price of the offered item in whole CZK. */
    val price: Int,
    /**
     * Prestige rating of the item.
     *
     * This value specified how much points are added to your entry on the prestige leaderboard when
     * you buy the item.
     *
     * This is not the only way of gaining prestige points.
     */
    val prestige: Int,
    /**
     * URL of an image of the item, if any.
     *
     * This is often some illustration image, not actual image of the item.
     */
    val imageUrl: String?,
    /** Whether the item is a permanent offer. */
    val isPermanentOffer: Boolean,
    /**
     * List of item labels.
     *
     * The items are categorized by textual labels. The labels are intended to be shown to
     * the users. Note that some labels are created only to server in the internal information
     * systems, not to the end-users.
     */
    val labels: List<String>,
)

