/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model

import cz.jwo.kachnastatus.model.dataobjects.*
import java.util.*

/**
 * Provides information about the current club state.
 *
 * @see ClubStateProvider to fetch club state information by date or ID.
 */
interface CurrentClubStateProvider {
    /**
     * Returns currently happening state in the club.
     */
    suspend fun getCurrentClubState(): ModelValueFlow<ClubState>
}

/**
 * Provides information about club states.
 *
 * @see CurrentClubStateProvider to get current club state.
 */
interface ClubStateProvider {
    /**
     * Returns list of club states between two
     */
    suspend fun getClubStatesBetween(startDate: Date, endDate: Date): ModelValueFlow<ClubStateList>

    /**
     * Returns current club state with the specified ID.
     *
     * See also the note on [Model.getEventsAndStatesBetween].
     */
    suspend fun getClubState(stateId: Int): ModelValueFlow<ClubState>
}

/**
 * Provides entries from the prestige leaderboard.
 */
interface PrestigeLeaderboardProvider {
    /**
     * Returns prestige leaderboard entries for the current day.
     */
    suspend fun getPrestigeLeaderboardToday(): ModelValueFlow<PrestigeInfo>

    /**
     * Returns prestige leaderboard entries fro the current semester.
     */
    suspend fun getPrestigeLeaderboardSemester(): ModelValueFlow<PrestigeInfo>
}

/**
 * Provides information about events in the club.
 *
 * @see CurrentEventListProvider
 */
interface EventProvider {
    /**
     * Returns event information for event with the specified ID.
     */
    suspend fun getEvent(eventId: Int): ModelValueFlow<Event>

    /**
     * Returns list of events occurring between two dates.
     *
     * See also the note on [Model.getEventsAndStatesBetween].
     */
    suspend fun getEventsBetween(startDate: Date, endDate: Date): ModelValueFlow<EventList>
}

/**
 * Provides list of currently happening events.
 */
interface CurrentEventListProvider {
    /**
     * Returns list of event happening at the moment.
     */
    suspend fun getCurrentEvents(): ModelValueFlow<EventList>
}

/**
 * Provides information about the currently offered products.
 */
interface CurrentOfferProvider {
    /**
     * Returns information about the currently offered items.
     */
    suspend fun getCurrentOffer(): ModelValueFlow<CurrentOffer>
}