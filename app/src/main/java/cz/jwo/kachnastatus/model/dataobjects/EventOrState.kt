/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import cz.jwo.kachnastatus.util.DateRange
import cz.jwo.kachnastatus.util.spansMultipleDays
import java.util.*

/**
 * An activity performed in the club.
 *
 * This is a superclass of only two classes: [Event] and [ClubState].
 *
 * The properties on this class provide information the is common to both activity types.
 */
sealed class EventOrState {
    /** Short description of the event or a club state. */
    abstract val shortDescription: String?

    /** Starting date and time of the event or a clu\b state. */
    abstract val from: Date

    /**
     * End date and time of the event or a club state.
     *
     * Note that events have this values always non-null.
     */
    abstract val to: Date?

    /**
     * Returns whether the event or club state spans multiple days.
     */
    val spansMultipleDays: Boolean
        get() = to.let { to ->
            if (to != null) DateRange(from, to).spansMultipleDays
            else false
        }
}