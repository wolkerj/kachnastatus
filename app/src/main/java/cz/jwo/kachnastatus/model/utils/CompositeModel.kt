/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.utils

import cz.jwo.kachnastatus.model.*
import cz.jwo.kachnastatus.model.dataobjects.*
import java.util.*

/**
 * A [Model] that delegates its operations to separate providers.
 *
 * The [CompositeModel] can be used to combine multiple providers into one model or to selectively
 * replace providers in an existing model.
 *
 * Replacing providers:
 *
 *   * Create an instance of [CompositeModel] using the constructor taking only [Model].
 *
 *     The instance uses the model to provide all calls.
 *
 *   * Replace providers (by directly setting the providers on the [CompositeModel]) in the model.
 */
class CompositeModel(
    var currentClubStateProvider: CurrentClubStateProvider,
    var clubStateProvider: ClubStateProvider,
    var prestigeLeaderboardProvider: PrestigeLeaderboardProvider,
    var currentEventListProvider: CurrentEventListProvider,
    var eventProvider: EventProvider,
    var currentOfferProvider: CurrentOfferProvider,
    // Note: Update setAllComponents implementation when adding new model components.
    // Note: Update also toString().
) : Model {
    /**
     * Creates new [CompositeModel] using the [model] to perform all operations.
     */
    constructor(model: Model) : this(model, model, model, model, model, model)

    override fun toString(): String =
        "${super.toString()}(" +
                "currentClubState: $currentClubStateProvider, " +
                "clubState: $clubStateProvider, " +
                "prestigeLeaderboard: $prestigeLeaderboardProvider, " +
                "event: $eventProvider, " +
                "currentEventList: $currentEventListProvider, " +
                "currentOffer: $currentOfferProvider)"

    override suspend fun getCurrentClubState(): ModelValueFlow<ClubState> =
        currentClubStateProvider.getCurrentClubState()

    override suspend fun getClubState(stateId: Int): ModelValueFlow<ClubState> =
        clubStateProvider.getClubState(stateId)

    override suspend fun getClubStatesBetween(startDate: Date, endDate: Date): ModelValueFlow<ClubStateList> =
        clubStateProvider.getClubStatesBetween(startDate, endDate)

    override suspend fun getPrestigeLeaderboardToday(): ModelValueFlow<PrestigeInfo> =
        prestigeLeaderboardProvider.getPrestigeLeaderboardToday()

    override suspend fun getPrestigeLeaderboardSemester(): ModelValueFlow<PrestigeInfo> =
        prestigeLeaderboardProvider.getPrestigeLeaderboardSemester()

    override suspend fun getEvent(eventId: Int): ModelValueFlow<Event> =
        eventProvider.getEvent(eventId)

    override suspend fun getEventsBetween(startDate: Date, endDate: Date): ModelValueFlow<EventList> =
        eventProvider.getEventsBetween(startDate, endDate)

    override suspend fun getCurrentEvents(): ModelValueFlow<EventList> =
        currentEventListProvider.getCurrentEvents()

    override suspend fun getCurrentOffer(): ModelValueFlow<CurrentOffer> =
        currentOfferProvider.getCurrentOffer()

    /**
     * Makes this [CompositeModel] use the specified model for all operations.
     */
    fun setAllComponents(model: Model) {
        currentClubStateProvider = model
        clubStateProvider = model
        prestigeLeaderboardProvider = model
        eventProvider = model
        currentEventListProvider = model
        currentOfferProvider = model
    }

    /**
     * Creates a shallow copy of the [CompositeModel].
     */
    fun copy() = CompositeModel(
        currentClubStateProvider = currentClubStateProvider,
        clubStateProvider = clubStateProvider,
        prestigeLeaderboardProvider = prestigeLeaderboardProvider,
        eventProvider = eventProvider,
        currentEventListProvider = currentEventListProvider,
        currentOfferProvider = currentOfferProvider,
    )
}

/**
 * Wraps the given model in a [CompositeModel] instance so its components can be modified.
 */
fun Model.toCompositeModel() = CompositeModel(this)