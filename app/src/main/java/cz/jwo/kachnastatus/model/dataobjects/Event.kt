/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.model.dataobjects

import cz.jwo.kachnastatus.util.ApiDateSerializer
import kotlinx.serialization.Serializable
import java.util.*

/**
 * An event reported by the Kachna Online app.
 *
 * Only non-standard activities of the club are reported as [Event]s. For ordinary club states like
 * “Open with bar” or “Chillzone”, see [ClubState] and related interfaces.
 *
 * When you want to show both events and club states in the UI, you can make use of the superclass
 * [EventOrState].
 *
 * Note that not all events happen in the club. See [place] and related values to show the location
 * of the events.
 */
@Serializable
data class Event(
    /** Name of the event. */
    val name: String,
    /** Place where the event is taking place. */
    val place: String?,
    /** URL to webpage with information about the place. */
    val placeUrl: String?,
    /**
     * URL of an illustration image.
     *
     * Note that the URL is relative to the app domain. Use [absoluteImageUrl] to get absolute URL.
     */
    val imageUrl: String?,
    /** Short description of the event */
    override val shortDescription: String,
    /** Full description of the event, if any */
    val fullDescription: String?,
    /** URL of a webpage with additional information about the event. */
    val url: String?,
    /** Event start date and time. */
    @Serializable(with = ApiDateSerializer::class)
    override val from: Date,
    /** Event end date and time. */
    @Serializable(with = ApiDateSerializer::class)
    override val to: Date,
    /* ID of the event. */
    val id: Int,
    /** IDs of club states related to the event, if any. */
    val linkedPlannedStateIds: List<Int>,
) : EventOrState() {
    /**
     * Absolute URL of the illustration image for the event page.
     *
     * The URL stored in [imageUrl] is a relative URL.
     */
    val absoluteImageUrl: String? = imageUrl?.let { "https://www.su.fit.vutbr.cz/${it.trimStart('/')}" }

    /**
     * Normalizes the event data.
     *
     * The event information is often misrepresented. This method cleans it up.
     *
     * @return new [Event] with fixed information
     */
    fun normalize(): Event = Event(
        name = name.trim(),
        place = place?.trim(),
        placeUrl = placeUrl?.trim()?.nullIfEmpty(),
        imageUrl = imageUrl?.trim()?.nullIfEmpty(),
        shortDescription = shortDescription.trim(),
        fullDescription = fullDescription?.trim()?.nullIfEmpty(),
        url = url?.trim()?.nullIfEmpty(),
        from = from, to = to,
        id = id,
        linkedPlannedStateIds = linkedPlannedStateIds,
    )
}

/**
 * Simply an [Event] list.
 *
 * This type alis is used to make signatures of API related functions easier to read.
 */
typealias EventList = List<Event>

/**
 * Returns `null` when the string is empty.
 */
private fun String.nullIfEmpty() = if (this.isEmpty()) null else this