/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.settings

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.util.Log
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

private const val TAG = "AppSettingsStorage"

/**
 * Handles on-disk storage of the app settings.
 *
 * Internally, Android SharedPreferences is used to store the settings. Since it was easier to
 * serialize the settings to JSON, only one entry (with the JSON) is stored in the shared prefs.
 */
class AppSettingsStorage(context: Context) {
    /**
     * Shared preferences store that is used to store the settings.
     */
    private val sharedPrefs = context.getSharedPreferences(SharedPrefsName, MODE_PRIVATE)

    /**
     * Reads the current settings from the storage.
     *
     * This is a blocking call.
     */
    fun getSettings(): AppSettings =
        sharedPrefs.getString(JsonPrefsKey, null)?.deserializeSettingsOrNull() ?: AppSettings.defaults

    /**
     * Stores the settings on the disk.
     *
     * The settings can be saved asynchronously.
     */
    fun saveSettings(appSettings: AppSettings) =
        with(sharedPrefs.edit()) {
            putString(JsonPrefsKey, serializeSettings(appSettings))
            apply()
        }

    /**
     * Deserializes the application settings from the on-disk format.
     */
    private fun String.deserializeSettings(): AppSettings =
        SettingsStringFormat.decodeFromString(this)

    /**
     * Deserializes the application settings or, if the deserialization fails, returns `null`.
     */
    private fun String.deserializeSettingsOrNull(): AppSettings? =
        try {
            deserializeSettings()
        } catch (exc: Exception) {
            Log.e(TAG, "Caught exception while decoding application settings:")
            exc.printStackTrace()
            null
        }

    /**
     * Serializes the application settings to the on-disk format.
     */
    private fun serializeSettings(settings: AppSettings): String =
        SettingsStringFormat.encodeToString(settings)

    companion object {
        /**
         * Name of the SharedPreferences store used to store the settings.
         */
        const val SharedPrefsName = "appSettings"

        /**
         * Key in the SharedPreferences store used to store the settings in JSON.
         */
        const val JsonPrefsKey = "appSettings"

        /**
         * Serialization format used to store the settings.
         */
        @OptIn(ExperimentalSerializationApi::class)
        private val SettingsStringFormat = Json {
            explicitNulls = true
            encodeDefaults = true
            classDiscriminator = "_type"
            prettyPrint = false
            ignoreUnknownKeys = true
        }
    }
}