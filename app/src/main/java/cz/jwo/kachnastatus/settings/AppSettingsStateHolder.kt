/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.settings

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import cz.jwo.kachnastatus.settings.AppSettings.Companion.defaults

private val defaults = AppSettings.defaults

/**
 * State holder usable with Compose UI that holds current application settings.
 *
 * The structure of the holder reflects [AppSettings].
 *
 * Methods for loading settings to the holder and creation of a [AppSettings] instance are provided.
 */
class AppSettingsStateHolder {
    var theme: ApplicationTheme by mutableStateOf(defaults.theme)
    var notifyBeforeOpening by mutableStateOf(defaults.notifyBeforeOpening)
    var notifyBeforeClosing by mutableStateOf(cz.jwo.kachnastatus.settings.defaults.notifyBeforeClosing)
    var permanentNotification by mutableStateOf(defaults.permanentNotification)
    var showPrivateEvents by mutableStateOf(defaults.showPrivateEvents)

    var calendarStyle by mutableStateOf(defaults.calendarStyle)

    var devOptionsVisible by mutableStateOf(defaults.devOptionsVisible)

    val developer = DeveloperSettings()

    /**
     * Resets values of all developer settings to defaults.
     */
    fun resetAllDevSettings() {
        loadDevSettingsFrom(defaults)
    }

    /**
     * Loads the settings from [AppSettings] instance to the holder.
     */
    fun loadSettingsFrom(settings: AppSettings) {
        loadDevSettingsFrom(settings)
        loadNonDevSettingsFrom(settings)
    }

    /**
     * Loads developer settings from [AppSettings] instance to the holder.
     */
    private fun loadDevSettingsFrom(settings: AppSettings) {
        developer.loadFrom(settings.developer)
    }

    /**
     * Creates new [AppSettings] instance with values reflecting this [AppSettingsStateHolder]
     * instance at the time of invocation.
     */
    fun toAppSettings() = AppSettings(
        theme = theme,
        notifyBeforeOpening = notifyBeforeOpening,
        notifyBeforeClosing = notifyBeforeClosing,
        permanentNotification = permanentNotification,
        showPrivateEvents = showPrivateEvents,
        calendarStyle = calendarStyle,
        devOptionsVisible = devOptionsVisible,
        developer = developer.toAppDevSettings(),
    )

    /**
     * Loads all but developer settings from the given [AppSettings] instance.
     */
    private fun loadNonDevSettingsFrom(settings: AppSettings) {
        theme = settings.theme
        notifyBeforeOpening = settings.notifyBeforeOpening
        notifyBeforeClosing = settings.notifyBeforeClosing
        permanentNotification = settings.permanentNotification
        showPrivateEvents = settings.showPrivateEvents
        calendarStyle = settings.calendarStyle
        devOptionsVisible = settings.devOptionsVisible
    }

    /**
     * State holder containing settings that are stored in [AppSettings.DeveloperSettings]
     */
    class DeveloperSettings {
        var mockPrestigeToday: AppSettings.DeveloperSettings.MockPrestigeStatePreset? by mutableStateOf(null)
        var mockPrestigeSemester: AppSettings.DeveloperSettings.MockPrestigeStatePreset? by mutableStateOf(null)

        var mockCurrentState: AppSettings.DeveloperSettings.MockClubStatePreset? by mutableStateOf(null)
        var mockFollowingState: AppSettings.DeveloperSettings.MockClubStatePreset? by mutableStateOf(null)
        val mockClubState
            get() = toAppDevSettings().mockClubState

        /**
         * Loads the settings from [AppSettings.DeveloperSettings] instance.
         */
        internal fun loadFrom(settings: AppSettings.DeveloperSettings) {
            mockPrestigeToday = settings.mockPrestigeToday
            mockPrestigeSemester = settings.mockPrestigeSemester
            mockCurrentState = settings.mockCurrentState
            mockFollowingState = settings.mockFollowingState
        }

        /**
         * Creates new instance of [AppSettings.DeveloperSettings] with values from this value
         * holder.
         */
        internal fun toAppDevSettings(): AppSettings.DeveloperSettings =
            AppSettings.DeveloperSettings(
                mockPrestigeToday = mockPrestigeToday,
                mockPrestigeSemester = mockPrestigeSemester,
                mockCurrentState = mockCurrentState,
                mockFollowingState = mockFollowingState,
            )
    }
}