/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.settings

import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.MockClubStates
import cz.jwo.kachnastatus.model.dataobjects.PrestigeInfo
import cz.jwo.kachnastatus.model.dataobjects.PrestigeItem
import kotlinx.serialization.Serializable
import java.time.Instant
import java.util.*

/**
 * Data class holding all application configuration values.
 *
 * This class can be serialized and stored to preserve the configuration values.
 *
 * @see AppSettingsStateHolder
 */
@Serializable
data class AppSettings(
    /** Currently selected application theme. */
    var theme: ApplicationTheme = ApplicationTheme.Default,

    /** Show notification before the club opens. */
    var notifyBeforeOpening: Boolean = true,
    /** Show notification before the club closes. */
    var notifyBeforeClosing: Boolean = false,
    /** Show a permanent notification with club state. */
    var permanentNotification: Boolean = true,
    /**
     * Show private events happening in the club
     *
     * When set to `false`, the app hides all private events in the user interface.
     */
    var showPrivateEvents: Boolean = false,

    /** Display style of the event calendar. */
    var calendarStyle: CalendarStyle = CalendarStyle.Weeks,

    /** Show developer options section in the settings. */
    var devOptionsVisible: Boolean = false,
    /** Application settings intended to be used only by the developers. */
    val developer: DeveloperSettings = DeveloperSettings(),
) {
    /**
     * Holds application settings that are intended to be used only by app developers.
     */
    @Serializable
    data class DeveloperSettings(
        var mockPrestigeToday: MockPrestigeStatePreset? = null,
        var mockPrestigeSemester: MockPrestigeStatePreset? = null,
        var mockCurrentState: MockClubStatePreset? = null,
        var mockFollowingState: MockClubStatePreset? = null,
    ) {
        val mockClubState
            get() = mockCurrentState?.eventData?.withFollowingState(
                mockFollowingState?.eventData?.copy(
                    start = Date.from(Instant.now().plusSeconds(3600))
                )
            )

        @Serializable
        enum class MockPrestigeStatePreset {
            Nobody, FakeEntries;

            val todayPrestigeOverride: PrestigeInfo
                get() =
                    when (this) {
                        Nobody -> emptyList()
                        FakeEntries -> listOf(
                            PrestigeItem("Kachnista", 4242),
                            PrestigeItem("PrestižníČlověk", 1234),
                            PrestigeItem("student", 65),
                            PrestigeItem("Děkan fakulty", 50),
                            PrestigeItem("Kolemjdoucí", 1),
                        )
                    }
            val semesterPrestigeOverride: PrestigeInfo
                get() =
                    when (this) {
                        Nobody -> emptyList()
                        FakeEntries -> listOf(
                            PrestigeItem("Kachnista", 12345),
                            PrestigeItem("PrestižníČlověk", 7500),
                            PrestigeItem("lorem", 1000),
                            PrestigeItem("Kolemjdoucí", 421),
                            PrestigeItem("ipsum", 50),
                        )
                    }
        }

        @Serializable
        enum class MockClubStatePreset {
            Closed, Private, Chillzone, Bar;

            val eventData: ClubState
                get() = when (this) {
                    Closed -> MockClubStates.closed
                    Private -> MockClubStates.private
                    Chillzone -> MockClubStates.openChillzone
                    Bar -> MockClubStates.openWithBar
                }
        }
    }

    companion object {
        /**
         * Default application settings.
         */
        val defaults = AppSettings()
    }
}