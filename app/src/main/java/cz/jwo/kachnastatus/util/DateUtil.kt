/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.util

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class DateRange(val from: Date, val to: Date) : Parcelable {
    override fun equals(other: Any?): Boolean {
        if (other !is DateRange?) return false;
        if (other == null) return false
        return other.from == from && other.to == to;
    }

    override fun hashCode(): Int {
        var result = from.hashCode()
        result = 31 * result + to.hashCode()
        return result
    }
}

/**
 * Returns UTC midnight of day in which the given date occurs.
 */
val Date.midnight: Date
    get() = Date(time - time % (24 * 3600_000))

/**
 * Returns UTC midnight that follow the given date.
 *
 * When used on a [Date] that already represents a midnight, returns the midnight after it.
 */
val Date.midnightAfter: Date
    get() = (time + 24 * 3600_000).let { time ->
        Date(time - time % (24 * 3600_000))
    }

/**
 * Returns whether the given date is an UTC midnight.
 */
val Date.isMidnight get() = this == midnight

/**
 * Returns the first UTC midnight occurring at or after the specified date.
 */
val Date.ceilToMidnight: Date
    get() = if (isMidnight) this else midnightAfter

/**
 * Returns list of all UTC midnight between two dates.
 *
 * @param start inclusive start of the range
 * @param end   exclusive end of the range
 */
fun midnightsBetween(start: Date, end: Date): List<Date> =
    (start.ceilToMidnight.time until end.midnight.time step 24 * 3600_000).map(::Date)
        .also { require(start.before(end)) { "start is after the end" } }

/**
 * Returns whether the date is strictly inside the specified time interval.
 */
infix fun Date.between(range: DateRange) =
    range.let { (start, end) ->
        after(start) && before(end)
            .also { require(start.before(end)) { "start is after the end" } }
    }

/**
 * Returns whether the two date ranges have a non-empty intersection.
 *
 * When one interval ends when the other begins, the intervals *do not* intersect (at least for use
 * by this function).
 */
infix fun DateRange.intersects(range: DateRange) =
    from between range || to between range

/**
 * Returns whether the specified time interval spans multiple days.
 */
val DateRange.spansMultipleDays: Boolean
    get() = from.midnight != to.midnight

/**
 * Returns UTC midnight of a Monday that in the week of the given date.
 */
val Date.weekStart: Date
    get() = GregorianCalendar().run {
        time = this@weekStart.midnight
        add(
            Calendar.DATE, when (get(Calendar.DAY_OF_WEEK)) {
                Calendar.MONDAY -> 0
                Calendar.TUESDAY -> -1
                Calendar.WEDNESDAY -> -2
                Calendar.THURSDAY -> -3
                Calendar.FRIDAY -> -4
                Calendar.SATURDAY -> -5
                Calendar.SUNDAY -> -6
                else -> throw AssertionError()
            }
        )
        time
    }

/**
 * Returns the first Monday UTC midnight after the given date.
 */
val Date.nextWeekStart: Date
    get() = (1..7).fold(weekStart) { acc, _ -> acc.midnightAfter }

/**
 * Formats the date in ISO 8601 format (date only).
 */
val Date.toIso8601Date: String
    get() =
        java.text.SimpleDateFormat("yyyy-MM-dd", Locale.US).format(this)