/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.util

import android.util.Log
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "DateSerializer"

private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US)
    .apply {
        isLenient = false
    }

/**
 * Kotlinx serialization serializer that uses the API format of the dates.
 *
 * The API uses RFC3339 date and time format with space between date and time and no time zone
 * information (CE(S)T is implied).
 */
class ApiDateSerializer : KSerializer<Date> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Date", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): Date =
        decoder.decodeString().let { timeToParse ->
            try {
                dateFormat.let { formatter ->
                    formatter.timeZone = TimeZone.getTimeZone("CET")
                    formatter.parse(timeToParse.split('.')[0])
                }
            } catch (exc: Exception) {
                Log.e(TAG, "Failed to parse date: “$timeToParse”")
                exc.printStackTrace()
                throw exc
            }
        }

    override fun serialize(encoder: Encoder, value: Date) =
        encoder.encodeString(
            dateFormat.format(value)
        )
}
