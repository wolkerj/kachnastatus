/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

/**
 * Performs a transformation on values in the [StateFlow].
 *
 * This function, unlike `map` on the [StateFlow] preserves all [StateFlow] behavior.
 */
fun <T, R> StateFlow<T>.mapStateFlow(
    scope: CoroutineScope = CoroutineScope(Dispatchers.Default),
    transform: (T) -> R,
): StateFlow<R> =
    MutableStateFlow(transform(this.value))
        .also {
            scope.launch {
                this@mapStateFlow.collect { newValue ->
                    it.value = transform(newValue)
                }
            }
        }