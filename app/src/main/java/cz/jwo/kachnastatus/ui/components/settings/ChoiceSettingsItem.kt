/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.components.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import cz.jwo.kachnastatus.R

@ExperimentalMaterial3Api
@Composable
fun <T> ChoiceSettingsItem(
    label: @Composable () -> Unit,
    value: T,
    values: Collection<T>,
    showValue: @Composable (T) -> Unit,
    onChange: (T) -> Unit,
    enabled: Boolean = true,
) {
    var showDialog by remember { mutableStateOf(false) }

    fun performChoice() {
        showDialog = true
    }

    SettingsItem(
        label = label,
        value = {
            InputChip(
                onClick = ::performChoice,
                label = { showValue(value) },
                selected = false,
                enabled = enabled,
            )
        },
        enabled = enabled,
        onClick = ::performChoice,
    )
    if (showDialog) {
        AlertDialog(title = { label() },
            onDismissRequest = { showDialog = false },
            confirmButton = { TextButton(onClick = { }) { Text(stringResource(R.string.button_cancel)) } },
            properties = DialogProperties(dismissOnBackPress = true, dismissOnClickOutside = true),
            text = {
                Column {
                    values.forEach { itemValue ->
                        Row(Modifier.padding(8.dp).clickable {
                            onChange(itemValue)
                            showDialog = false
                        }) {
                            RadioButton(selected = value == itemValue, onClick = null)
                            Spacer(Modifier.width(16.dp))
                            Row(Modifier.weight(1.0f)) {
                                showValue(itemValue)
                            }
                        }
                    }
                }
            })
    }
}