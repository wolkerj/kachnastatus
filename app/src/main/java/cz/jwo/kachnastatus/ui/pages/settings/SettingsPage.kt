/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(
    ExperimentalMaterial3Api::class
)

package cz.jwo.kachnastatus.ui.pages.settings

import android.app.AlertDialog
import android.content.Intent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.activities.LegalInfoActivity
import cz.jwo.kachnastatus.settings.AppSettings
import cz.jwo.kachnastatus.settings.AppSettingsStateHolder.*
import cz.jwo.kachnastatus.settings.ApplicationTheme
import cz.jwo.kachnastatus.ui.LocalAppSettings
import cz.jwo.kachnastatus.ui.components.settings.ButtonSettingsItem
import cz.jwo.kachnastatus.ui.components.settings.ChoiceSettingsItem
import cz.jwo.kachnastatus.ui.components.settings.SwitchSettingsItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsPage(navController: NavHostController) {
    val appSettings = LocalAppSettings.current
    val context = LocalContext.current

    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text(stringResource(R.string.settingsPage_title)) },
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(
                            painterResource(R.drawable.round_arrow_back_24),
                            contentDescription = stringResource(R.string.action_back)
                        )
                    }
                },
                modifier = Modifier.windowInsetsPadding(WindowInsets.safeDrawing.only(WindowInsetsSides.Horizontal + WindowInsetsSides.Top)),
            )
        },
        contentWindowInsets = WindowInsets.safeContent.only(WindowInsetsSides.Top + WindowInsetsSides.Horizontal),
    ) { paddingValues ->
        Column(
            Modifier.padding(paddingValues).verticalScroll(rememberScrollState()).fillMaxHeight()
        ) {
            Text(
                stringResource(R.string.settingsPage_workInProgress),
                style = MaterialTheme.typography.headlineSmall,
                textAlign = TextAlign.Center,
                softWrap = true,
                modifier = Modifier.fillMaxWidth().padding(horizontal = 24.dp, vertical = 0.dp)
            )

            ChoiceSettingsItem(
                label = { Text(stringResource(R.string.setting_theme_label)) },
                value = appSettings.theme.let {
                    if (it == ApplicationTheme.Default) ApplicationTheme.Light
                    else it
                },
                showValue = {
                    Text(
                        stringResource(
                            when (it) {
                                ApplicationTheme.Light -> R.string.setting_theme_light
                                ApplicationTheme.Default -> R.string.setting_theme_light
                                ApplicationTheme.Dark -> R.string.setting_theme_dark
                                ApplicationTheme.FollowSystem -> R.string.setting_theme_followSystem
                            }
                        )
                    )
                },
                values = ApplicationTheme.values().filter { it != ApplicationTheme.Default }.toList(),
                onChange = { newValue ->
                    appSettings.theme = newValue
                },
            )
            SwitchSettingsItem(
                label = { Text(stringResource(R.string.setting_notifyBeforeOpening)) },
                checked = appSettings.notifyBeforeOpening,
                onChange = { newValue ->
                    appSettings.notifyBeforeOpening = newValue
                },
                enabled = false,
            )
            SwitchSettingsItem(
                label = { Text(stringResource(R.string.setting_notifyBeforeClosing)) },
                checked = appSettings.notifyBeforeClosing,
                onChange = { newValue ->
                    appSettings.notifyBeforeClosing = newValue
                },
                enabled = false,
            )
            SwitchSettingsItem(
                label = { Text(stringResource(R.string.setting_permanentNotification)) },
                checked = appSettings.permanentNotification,
                onChange = { newValue ->
                    appSettings.permanentNotification = newValue
                },
                enabled = false,
            )
            SwitchSettingsItem(
                label = { Text(stringResource(R.string.setting_showPrivateEvents)) },
                checked = appSettings.showPrivateEvents,
                onChange = { newValue ->
                    appSettings.showPrivateEvents = newValue
                },
            )
            SwitchSettingsItem(
                label = { Text(stringResource(R.string.setting_developerMode)) },
                checked = appSettings.devOptionsVisible,
                onChange = { newValue ->
                    if (newValue) {
                        AlertDialog.Builder(context)
                            .setTitle(R.string.setting_developerMode_warning_title)
                            .setMessage(R.string.setting_developerMode_warning_text)
                            .setPositiveButton(R.string.button_yes) { _, _ -> appSettings.devOptionsVisible = true }
                            .setNegativeButton(R.string.button_no) { _, _ -> }
                            .show()
                    } else {
                        appSettings.devOptionsVisible = false
                        appSettings.resetAllDevSettings()
                    }
                },
            )

            AnimatedVisibility(appSettings.devOptionsVisible) {
                Column {
                    Spacer(Modifier.height(24.dp))
                    Text(
                        stringResource(R.string.setting_developerSection),
                        style = MaterialTheme.typography.headlineLarge,
                        modifier = Modifier.padding(horizontal = 16.dp)
                    )

                    @Composable
                    fun AppSettings.DeveloperSettings.MockPrestigeStatePreset?.Label() {
                        Text(
                            stringResource(
                                when (this) {
                                    null -> R.string.setting_devMockPrestige_disabled
                                    AppSettings.DeveloperSettings.MockPrestigeStatePreset.Nobody -> R.string.setting_devMockPrestige_nobody
                                    AppSettings.DeveloperSettings.MockPrestigeStatePreset.FakeEntries -> R.string.setting_devMockPrestige_fakeEntries
                                }
                            )
                        )
                    }
                    ChoiceSettingsItem(
                        label = { Text(stringResource(R.string.setting_devMockPrestigeToday)) },
                        value = appSettings.developer.mockPrestigeToday,
                        values = listOf(null) + AppSettings.DeveloperSettings.MockPrestigeStatePreset.values()
                            .toList(),
                        showValue = { it.Label() },
                        onChange = {
                            appSettings.developer.mockPrestigeToday = it
                        }
                    )
                    ChoiceSettingsItem(
                        label = { Text(stringResource(R.string.setting_devMockPrestigeSemester)) },
                        value = appSettings.developer.mockPrestigeSemester,
                        values = listOf(null) + AppSettings.DeveloperSettings.MockPrestigeStatePreset.values()
                            .toList(),
                        showValue = { it.Label() },
                        onChange = {
                            appSettings.developer.mockPrestigeSemester = it
                        }
                    )

                    @Composable
                    fun AppSettings.DeveloperSettings.MockClubStatePreset?.Label() {
                        Text(
                            stringResource(
                                when (this) {
                                    null -> R.string.setting_devMockClubState_disabled
                                    AppSettings.DeveloperSettings.MockClubStatePreset.Private -> R.string.setting_devMockClubState_private
                                    AppSettings.DeveloperSettings.MockClubStatePreset.Chillzone -> R.string.setting_devMockClubState_openChillzone
                                    AppSettings.DeveloperSettings.MockClubStatePreset.Bar -> R.string.setting_devMockClubState_openWithBar
                                    AppSettings.DeveloperSettings.MockClubStatePreset.Closed -> R.string.setting_devMockClubState_closed
                                }
                            )
                        )
                    }
                    ChoiceSettingsItem(
                        label = { Text(stringResource(R.string.setting_devMockCurrentState)) },
                        value = appSettings.developer.mockCurrentState,
                        values = listOf(null) + AppSettings.DeveloperSettings.MockClubStatePreset.values()
                            .toList(),
                        showValue = { it.Label() },
                        onChange = {
                            appSettings.developer.mockCurrentState = it
                        }
                    )
                    ChoiceSettingsItem(
                        label = { Text(stringResource(R.string.setting_devMockFollowingState)) },
                        value = appSettings.developer.mockFollowingState,
                        values = listOf(null) + AppSettings.DeveloperSettings.MockClubStatePreset.values()
                            .toList(),
                        showValue = { it.Label() },
                        onChange = {
                            appSettings.developer.mockFollowingState = it
                        },
                        enabled = appSettings.developer.mockCurrentState != null,
                    )
                }
            }

            ButtonSettingsItem(
                {
                    context.startActivity(
                        Intent(
                            context,
                            LegalInfoActivity::class.java
                        ).putExtra(LegalInfoActivity.CAN_GO_UP, true)
                    )
                },
            ) { Text(stringResource(R.string.navigate_legalInformation)) }
        }
    }
}
