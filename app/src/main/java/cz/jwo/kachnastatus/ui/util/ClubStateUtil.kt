/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.StateType


private val ClubState.guiLabelResource: Int
    get() = when (this.type) {
        StateType.OpenBar -> R.string.clubState_openBar
        StateType.OpenChillzone -> R.string.clubState_openChillzone
        StateType.Private -> R.string.clubState_private
        StateType.Closed -> R.string.clubState_closed
    }

@Composable
fun ClubState.getGuiLabel(): String =
    stringResource(guiLabelResource)