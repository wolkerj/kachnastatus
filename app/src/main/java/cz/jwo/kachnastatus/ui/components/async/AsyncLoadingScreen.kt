/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.components.async

import android.annotation.SuppressLint
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

/**
 * Simple UI that can be used to show asynchronously-loaded data.
 *
 * When the data is being loaded, [loadingContent] is used.
 *
 * If the loading terminates with an error, [errorContent] is used. The default [errorContent] shows
 * stacktrace and brief information about the error. When the default screen is used, you can use
 * [errorScreenModifier] to modify its composition. (The [errorScreenModifier] does not have any
 * effect when custom [errorContent] is used.)
 */
@Composable
fun <T> AsyncLoadingScreen(
    loadResult: AsynchronousLoadResult<T>,
    @SuppressLint("ModifierParameter") errorScreenModifier: Modifier = Modifier,
    errorContent: @Composable (Exception) -> Unit = { ErrorScreen(it, modifier = errorScreenModifier) },
    loadingContent: @Composable () -> Unit,
    content: @Composable (T) -> Unit,
) {
    if (loadResult.isError) {
        errorContent(loadResult.error!!)
    } else if (loadResult.isDone) {
        content(loadResult.value!!)
    } else {
        loadingContent()
    }
}