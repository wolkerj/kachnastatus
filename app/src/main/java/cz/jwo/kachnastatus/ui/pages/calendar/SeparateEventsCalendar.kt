/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.calendar

import android.os.Build
import android.text.format.DateFormat
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.ui.components.calendar.CalendarEntryCard
import cz.jwo.kachnastatus.util.DateRange
import cz.jwo.kachnastatus.util.midnight
import cz.jwo.kachnastatus.util.spansMultipleDays
import kotlinx.coroutines.flow.StateFlow
import java.util.*

/**
 * Calendar view that shows all events in a column and with more details that [WeeksCalendar].
 */
@Composable
internal fun SeparateEventsCalendar(
    eventsFlow: StateFlow<ModelResponse<List<EventOrState>>>,
    padding: PaddingValues = PaddingValues(0.dp),
) {
    val events = eventsFlow.collectAsState().value.value

    /** List of events shown, grouped by the day (or day range, for multi-day events). */
    val eventsByDay = events
        .groupBy { DateRange(it.from.midnight, it.to?.midnight ?: it.from.midnight) }.toList()
        // Sort the ranges by their start date (and place multi-day events above single-day events).
        .sortedWith { (_, leftEvents), (_, rightEvents) ->
            val leftEvent = leftEvents[0]
            val rightEvent = rightEvents[0]
            compareValuesBy(leftEvent, rightEvent) { it.from.midnight }
                .let { firstComparisonResult ->
                    if (firstComparisonResult == 0) {
                        compareValuesBy(leftEvent, rightEvent) { !it.spansMultipleDays }
                    } else firstComparisonResult
                }
        }
        // Sort the events in the groups by their start time.
        .map { (dateRange, events) -> DayEvents(dateRange, events.sortedBy { it.from }) }

    LazyColumn(contentPadding = padding) {
        // We chunk the event groups for better performance of the LazyColumn.
        items(eventsByDay.chunked(16), key = { it.first().day.from }) { eventsByDayChunk ->
            eventsByDayChunk.forEach { (dateRange, events) ->
                SeparateDaysCalendarEventHeader(dateRange)
                events.forEach { event ->
                    CalendarEntryCard(event)
                    Spacer(Modifier.height(8.dp))
                }
            }
        }
    }
}

/**
 * Header shown before each day's events.
 */
@Composable
private fun SeparateDaysCalendarEventHeader(dateRange: DateRange) {
    val (from, to) = dateRange
    val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        LocalConfiguration.current.locales[0]
    } else {
        LocalConfiguration.current.locale
    }
    val dateFormatter =
        DateFormat.getBestDateTimePattern(locale, "ddMMMMM")
            .let { pattern ->
                java.text.SimpleDateFormat(pattern, locale)
            }

    fun Date.format(): String = dateFormatter.format(this)

    Spacer(Modifier.height(12.dp))
    Box(Modifier.padding(horizontal = 24.dp)) {
        Text(buildAnnotatedString {
            withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                if (dateRange.spansMultipleDays) {
                    append(from.format())
                    append("–") // n-dash
                    append(to.format())
                } else {
                    append(from.format())
                }
            }
        })
    }
    Spacer(Modifier.height(8.dp))
}