/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.main

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.dataobjects.PrestigeInfo

@Composable
internal fun PrestigeBlock(prestige: MainPageData.Prestige) {
    val prestigeToday = prestige.prestigeToday.collectAsState().value.value
    val prestigeSemester = prestige.prestigeSemester.collectAsState().value.value

    Card(modifier = Modifier.fillMaxWidth().padding(16.dp)) {
        Column(Modifier.padding(vertical = 8.dp, horizontal = 16.dp)) {
            Text(
                "Žebříček prestiže",
                modifier = Modifier.align(Alignment.CenterHorizontally),
                style = MaterialTheme.typography.titleLarge
            )
            Spacer(Modifier.height(8.dp))
            BoxWithConstraints {
                if (maxWidth < 420.dp / LocalConfiguration.current.fontScale) {
                    Column(verticalArrangement = Arrangement.spacedBy(8.dp)) {
                        if (prestigeToday.isEmpty() && prestigeSemester.isEmpty()) {
                            Text(
                                stringResource(R.string.mainPage_prestige_nobodySemester),
                                textAlign = TextAlign.Center,
                                modifier = Modifier
                                    .padding(16.dp),
                            )
                        } else {
                            if (prestigeToday.isNotEmpty()) {
                                PrestigeTable(
                                    title = { Text(stringResource(R.string.mainPage_prestige_today_title)) },
                                    prestigeToday,
                                )
                            } else {
                                Text(
                                    stringResource(R.string.mainPage_prestige_nobodyToday),
                                    textAlign = TextAlign.Center,
                                    modifier = Modifier
                                        // Move it slightly up.
                                        .padding(bottom = 12.dp)
                                )
                            }
                            PrestigeTable(
                                title = { Text(stringResource(R.string.mainPage_prestige_semester_title)) },
                                prestigeSemester,
                            )
                        }
                    }
                } else {
                    Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                        if (prestigeToday.isEmpty() && prestigeSemester.isEmpty()) {
                            Text(
                                stringResource(R.string.mainPage_prestige_nobodySemester),
                                textAlign = TextAlign.Center,
                                modifier = Modifier
                                    .weight(1.0f)
                                    .padding(16.dp),
                            )
                        } else {
                            if (prestigeToday.isNotEmpty()) {
                                PrestigeTable(
                                    title = { Text(stringResource(R.string.mainPage_prestige_today_title)) },
                                    prestigeToday,
                                    modifier = Modifier.weight(1.0f)
                                )
                            } else {
                                Text(
                                    stringResource(R.string.mainPage_prestige_nobodyToday),
                                    textAlign = TextAlign.Center,
                                    modifier = Modifier
                                        .weight(1.0f)
                                        .align(Alignment.CenterVertically)
                                        // Move it slightly up.
                                        .padding(bottom = 12.dp)
                                )
                            }
                            PrestigeTable(
                                title = { Text(stringResource(R.string.mainPage_prestige_semester_title)) },
                                prestigeSemester,
                                modifier = Modifier.weight(1.0f)
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
internal fun PrestigeTable(title: @Composable () -> Unit, prestigeInfo: PrestigeInfo, modifier: Modifier = Modifier) {
    Column(modifier) {
        Box(Modifier.align(Alignment.CenterHorizontally)) { title() }
        Divider(Modifier.fillMaxWidth())
        for ((position, prestigeItem) in prestigeInfo.iterator().withIndex()) {
            Row(Modifier.fillMaxWidth()) {
                Text(
                    modifier = Modifier.defaultMinSize(
                        minWidth = 24.dp * LocalConfiguration.current.fontScale,
                        minHeight = 0.dp
                    ),
                    textAlign = TextAlign.Right,
                    text = "${position + 1}. "
                )
                Text(
                    modifier = Modifier.weight(1.0f),
                    text = prestigeItem.nickname
                )
                Text(text = prestigeItem.prestige.toString())
            }
        }
    }
}
