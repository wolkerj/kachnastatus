/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.theme

import android.app.Activity
import android.content.res.Configuration
import android.graphics.Color.TRANSPARENT
import android.os.Build
import android.view.WindowManager
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.ViewCompat

private val DarkColorScheme = darkColorScheme(
    primary = Color(0xffaaaaa00),
    secondary = Color(0xffaa8800),
    onPrimary = Color.White,
    primaryContainer = Color(0xff774400),
    tertiaryContainer = Color(0xff000044),
    secondaryContainer = Color(0xff336688),
    onSecondaryContainer = Color.White,
    surface = Color.DarkGray,
    onSurface = Color.White,
    background = Color.Black,
    onBackground = Color.White,
)

private val LightColorScheme = lightColorScheme(
    primary = Yellow,
    onPrimary = Color(0xaa000000),
    secondary = Color(0xff77ee77),
    tertiary = Pink40,
    primaryContainer = Color(0xffeee088),

    /* Other default colors to override
    background = Color(0xFFFFFBFE),
    surface = Color(0xFFFFFBFE),
    onPrimary = Color.White,
    onSecondary = Color.White,
    onTertiary = Color.White,
    onBackground = Color(0xFF1C1B1F),
    onSurface = Color(0xFF1C1B1F),
    */
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun KachnaStatusTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = false,
    bottomSheetShown: Boolean,
    content: @Composable () -> Unit,
) {
    val scrimColor = BottomSheetDefaults.ScrimColor

    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val windowInsetsController = ViewCompat.getWindowInsetsController(view)!!
            val window = (view.context as Activity).window

            // Navigation bar.
            if (view.context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT || bottomSheetShown) {
                window.navigationBarColor = TRANSPARENT
                windowInsetsController.isAppearanceLightNavigationBars = !darkTheme
            } else { // Draw the navigation bar semi-opaque when in landscape or when a bottom sheet is shown.
                window.navigationBarColor = scrimColor.toArgb()
                windowInsetsController.isAppearanceLightNavigationBars = false
            }

            // Status bar.
            window.statusBarColor = TRANSPARENT
            windowInsetsController.isAppearanceLightStatusBars = !darkTheme

            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
    ) {
        CompositionLocalProvider(LocalContentColor provides colorScheme.onBackground) {
            content()
        }
    }
}
