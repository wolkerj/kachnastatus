/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.main

import android.text.format.DateFormat
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.EventList
import cz.jwo.kachnastatus.model.dataobjects.StateType
import cz.jwo.kachnastatus.ui.LocalAppSettings
import cz.jwo.kachnastatus.ui.util.getGuiLabel
import cz.jwo.kachnastatus.ui.util.letIf

@Composable
internal fun StatusBlock(clubStateFlow: ModelValueFlow<ClubState>, currentEventsFlow: ModelValueFlow<EventList>) {
    val clubState = clubStateFlow.collectAsState().value.value
        .letIf(!LocalAppSettings.current.showPrivateEvents) { it.toClosedIfPrivate() }
    val currentEvents = currentEventsFlow.collectAsState().value.value
    val showDetails = LocalAppSettings.current.showPrivateEvents || clubState.type != StateType.Private

    val timeFormat = DateFormat.getTimeFormat(LocalContext.current)
    val dateTimeFormat =
        java.text.DateFormat.getDateTimeInstance(
            /* dateStyle = */ java.text.DateFormat.MEDIUM,
            /* timeStyle = */ java.text.DateFormat.SHORT
        )

    Text(
        if (clubState.isClosed && currentEvents.isNotEmpty()) {
            stringResource(R.string.clubState_eventOnly)
        } else {
            clubState.getGuiLabel()
        }, style = MaterialTheme.typography.headlineLarge
    )

    if (showDetails) {
        if (clubState.isOpen) {
            Text(
                if (clubState.plannedEnd != null) {
                    "from %s to %s".format(
                        timeFormat.format(clubState.start),
                        timeFormat.format(clubState.plannedEnd)
                    )
                } else {
                    "from %s".format(timeFormat.format(clubState.start))
                }, style = MaterialTheme.typography.headlineSmall
            )
        }
    }

    if (clubState.isClosed && clubState.followingState != null
        && clubState.followingState.isOpen
        && (clubState.followingState.type != StateType.Private || LocalAppSettings.current.showPrivateEvents)
    ) {
        Text(
            when (clubState.followingState.type) {
                StateType.Closed -> throw AssertionError("the following (opening) state to be shown is Closed")
                StateType.OpenBar -> stringResource(R.string.mainPage_followingState_openBar)
                StateType.OpenChillzone -> stringResource(R.string.mainPage_followingState_openChillzone)
                StateType.Private -> stringResource(R.string.mainPage_followingState_privateEvent)
            }
        )
        Text(
            dateTimeFormat.format(clubState.followingState.start),
            style = MaterialTheme.typography.headlineSmall
        )
        clubState.followingState.creator?.let {
            Text(buildAnnotatedString {
                withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(stringResource(R.string.mainPage_madeByUser_label))
                }
                append(it.completeName)
            })
        }
        clubState.followingState.note?.let {
            Text(it, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth(.8f))
        }
    }

    if (clubState.creator != null) {
        Text(buildAnnotatedString {
            withStyle(SpanStyle(fontWeight = FontWeight.Bold)) { append(stringResource(R.string.mainPage_madeByUser_label)) }
            append(clubState.creator.completeName)
        })
    }
    
    if (!clubState.note?.trim().isNullOrEmpty()) {
        Text(clubState.note!!, modifier = Modifier.fillMaxWidth(.8f), textAlign = TextAlign.Center)
    }
}