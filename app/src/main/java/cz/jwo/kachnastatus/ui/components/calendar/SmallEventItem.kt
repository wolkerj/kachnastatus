/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.components.calendar

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.Event
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.ui.getAssociatedColor
import cz.jwo.kachnastatus.ui.util.getGuiLabel

@Composable
fun SmallEventItem(event: EventOrState, modifier: Modifier = Modifier) {
    Surface(
        modifier = Modifier.then(modifier),
        tonalElevation = 0.dp,
        shadowElevation = 0.dp,
        shape = MaterialTheme.shapes.extraSmall,
        color = event.getAssociatedColor()
    ) {
        val label = when (event) {
            is Event -> event.name
            is ClubState -> event.getGuiLabel()
        }
        Box(Modifier.padding(4.dp, 2.dp)) {
            Text(
                text = label,
                maxLines = 1,
                style = LocalTextStyle.current.run {
                    copy(
                        fontSize = fontSize * .8f
                    )
                },
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}