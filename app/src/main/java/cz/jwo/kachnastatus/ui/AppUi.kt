/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
    ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class,
)

package cz.jwo.kachnastatus.ui

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import cz.jwo.kachnastatus.nav.ModalWindowManager
import cz.jwo.kachnastatus.nav.NavDestination
import cz.jwo.kachnastatus.ui.pages.calendar.CalendarPage
import cz.jwo.kachnastatus.ui.pages.currentoffer.CurrentOfferPage
import cz.jwo.kachnastatus.ui.pages.main.MainPage
import cz.jwo.kachnastatus.ui.pages.settings.SettingsPage
import kotlinx.coroutines.launch

/**
 * List of all items in the bottom navigation bar.
 */
private val bottomNavigationItems = listOf(
    NavDestination.Main,
    NavDestination.CurrentOffer,
    NavDestination.Calendar,
    NavDestination.Settings,
)

/**
 * Builds a navigation graph for the application-wide [NavHostController].
 */
private fun NavGraphBuilder.buildNavGraph(navigationController: NavHostController) {
    composable(NavDestination.Main.route) { MainPage() }
    composable(NavDestination.CurrentOffer.route) { CurrentOfferPage() }
    composable(NavDestination.Calendar.route) { CalendarPage() }
    composable(NavDestination.Settings.route) { SettingsPage(navigationController) }
}

/**
 * A [Composable] with the whole application UI.
 */
@Composable
fun AppUi(modalWindowManager: ModalWindowManager) {
    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination
    val coroutineScope = rememberCoroutineScope()

    NavControllerProvider(navController) {
        modalWindowManager.modalDialogs.forEach { dialogContent ->
            Dialog(
                onDismissRequest = { modalWindowManager.closeModalDialog() },
            ) {
                Surface(
                    shape = MaterialTheme.shapes.large,
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(.9f)
                ) {
                    Column(Modifier.verticalScroll(rememberScrollState())) {
                        Spacer(Modifier.height(16.dp))
                        dialogContent.Composable(Modifier)
                        Spacer(Modifier.height(16.dp))
                    }
                }
            }
        }

        Column(Modifier) {
            NavHost(
                navController,
                startDestination = NavDestination.Main.route,
                modifier = Modifier.weight(1.0f)
            ) {
                buildNavGraph(navController)
            }

            NavigationBar(
                tonalElevation = 48.dp,
                windowInsets = WindowInsets.safeDrawing.only(WindowInsetsSides.Bottom + WindowInsetsSides.Horizontal)
            ) {
                bottomNavigationItems.forEach { item ->
                    NavigationBarItem(
                        label = {
                            Text(
                                stringResource(item.name),
                                overflow = TextOverflow.Ellipsis,
                                softWrap = false,
                            )
                        },
                        selected = currentDestination?.hierarchy?.any { it.route == item.route } ?: false,
                        icon = {
                            Icon(
                                painterResource(item.icon),
                                tint = LocalContentColor.current,
                                contentDescription = null
                            )
                        },
                        onClick = {
                            navController.navigate(item.route) {
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        }
                    )
                }
            }
        }

        if (modalWindowManager.bottomSheetShown) {
            BackHandler {
                coroutineScope.launch { modalWindowManager.closeBottomSheet() }
            }
            ModalBottomSheet(
                containerColor = MaterialTheme.colorScheme.surface,
                contentColor = MaterialTheme.colorScheme.onSurface,
                content = {
                    Column(
                        Modifier
                            .heightIn(max = Dp(LocalConfiguration.current.screenHeightDp.toFloat()) * .8f)
                            .verticalScroll(rememberScrollState()),
                    ) {
                        modalWindowManager.currentBottomSheet.let { bottomSheetRoute ->
                            if (bottomSheetRoute != null) {
                                bottomSheetRoute.Composable(Modifier)
                            } else {
                                // The initial/empty bottom sheet must contain at least something, otherwise the composition fails.
                                Spacer(Modifier.size(1.dp))
                            }

                            // Pad to compensate the bottom insets.
                            with(LocalDensity.current) {
                                Spacer(
                                    Modifier.height(
                                        WindowInsets.safeDrawing.getBottom(this).toDp()
                                    )
                                )
                            }
                        }
                    }
                },
                sheetState = modalWindowManager.sheetState,
                onDismissRequest = {
                    coroutineScope.launch { modalWindowManager.closeBottomSheet() }
                },
            )
        }
    }
}
