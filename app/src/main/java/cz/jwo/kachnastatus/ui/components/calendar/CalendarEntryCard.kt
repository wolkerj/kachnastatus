/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package cz.jwo.kachnastatus.ui.components.calendar

import android.text.format.DateFormat
import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.Event
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.nav.ModalWindowContent
import cz.jwo.kachnastatus.ui.LocalModalWindowManager
import cz.jwo.kachnastatus.ui.cardColorsForEvent
import cz.jwo.kachnastatus.ui.pages.calendar.TAG
import cz.jwo.kachnastatus.ui.util.getGuiLabel
import kotlinx.coroutines.launch

@Composable
fun CalendarEntryCard(event: EventOrState) {
    val calendarEntryCardTime = MaterialTheme.typography.titleSmall
    val calendarEntryCardHeadline = MaterialTheme.typography.titleLarge

    val composeScope = rememberCoroutineScope()
    val modalWindowManager = LocalModalWindowManager.current

    @Composable
    fun EventDateLabel(event: EventOrState) {
        val timeFormat = DateFormat.getTimeFormat(LocalContext.current)
        Text(style = calendarEntryCardTime,
            text = buildAnnotatedString {
                event.to.let { endTime ->
                    if (endTime != null) {
                        append(timeFormat.format(event.from))
                        append("–") // n-dash
                        append(timeFormat.format(endTime))
                    } else {
                        append(stringResource(R.string.eventInfo_fromNoUntil, timeFormat.format(event.from)))
                    }
                }
            }
        )
    }

    @Composable
    fun ShortEventInfo(event: Event) {
        EventDateLabel(event)
        Text(
            event.name, style = calendarEntryCardHeadline,
            maxLines = 2, overflow = TextOverflow.Ellipsis
        )
        if (event.shortDescription.isNotEmpty()) {
            Spacer(Modifier.height(8.dp))
            Text(
                event.shortDescription,
                maxLines = 2, overflow = TextOverflow.Ellipsis
            )
        }
    }

    @Composable
    fun ShortClubStateInfo(clubState: ClubState) {
        EventDateLabel(clubState)
        Text(clubState.getGuiLabel(), style = calendarEntryCardHeadline)
    }

    Box(Modifier.padding(horizontal = 16.dp)) {
        Card(
            // We override the colors and elevation to make disabled (non-clickable) cards look the same as enabled.
            colors = cardColorsForEvent(event),
            elevation = CardDefaults.cardElevation(
                disabledElevation = 0.dp, // same as default for filled cards
            ),
            enabled = event is Event,
            onClick = {
                when (event) {
                    is Event -> composeScope.launch {
                        modalWindowManager.showModal(
                            ModalWindowContent.EventDetails(
                                event.id
                            )
                        )
                    }

                    is ClubState -> {}
                }
            },
        ) {
            Row(Modifier.fillMaxWidth()) {
                val infoPartWeight = 3.0f
                Column(Modifier.weight(infoPartWeight).padding(16.dp, 16.dp)) {
                    when (event) {
                        is Event -> ShortEventInfo(event)
                        is ClubState -> ShortClubStateInfo(event)
                    }
                }
                if (event is Event && event.imageUrl != null) {
                    EventImage(event, modifier = Modifier.weight(2.0f))
                }
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun EventImage(event: Event, modifier: Modifier = Modifier) {
    Log.d(TAG, "Will use image from this URL: ${event.absoluteImageUrl}")
    // TODO Use Glide support for LazyListState.
    GlideImage(
        model = event.absoluteImageUrl, contentDescription = null,
        contentScale = ContentScale.FillWidth,
        alignment = Alignment.TopEnd,
        modifier = modifier,
    )
}