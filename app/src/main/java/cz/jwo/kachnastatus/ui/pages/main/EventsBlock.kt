/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.main

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.EventList
import cz.jwo.kachnastatus.ui.components.calendar.CalendarEntryCard

@Composable
internal fun EventsBlock(
    currentEventsFlow: ModelValueFlow<EventList>,
) {
    val currentEvents = currentEventsFlow.collectAsState().value.value

    currentEvents.forEach { event ->
        Spacer(Modifier.height(16.dp))
        CalendarEntryCard(event)
    }

    if (currentEvents.isNotEmpty()) {
        Spacer(Modifier.height(8.dp))
    }
}