/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.modals

import android.text.format.DateFormat
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.Event
import cz.jwo.kachnastatus.ui.components.async.AsyncLoadingScreen
import cz.jwo.kachnastatus.ui.components.async.AsynchronouslyLoaded
import cz.jwo.kachnastatus.ui.getModel
import cz.jwo.kachnastatus.ui.util.openLink
import java.util.*

@Composable
fun EventDetailsModal(eventId: Int, modifier: Modifier = Modifier) {
    val model = getModel()

    AsynchronouslyLoaded(
        { model.getEvent(eventId) }
    ) { loadResult ->
        AsyncLoadingScreen(
            loadResult,
            loadingContent = { Text("Loading…") },
            errorScreenModifier = modifier,
        ) { event ->
            Column(modifier) {
                EventDetailsModalContent(event)
            }
        }
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
private fun EventDetailsModalContent(eventFlow: ModelValueFlow<Event>) {
    val context = LocalContext.current

    val event = eventFlow.collectAsState().value.value

    val dateFormat = android.icu.text.SimpleDateFormat(
        DateFormat.getBestDateTimePattern(Locale.getDefault(), "HHmmEddMMMM"),
        Locale.getDefault()
    )

    fun Date.format() = dateFormat.format(this)

    Row(Modifier.fillMaxWidth()) {
        Column(Modifier.weight(3.0f).padding(horizontal = 16.dp)) {
            Text(event.name, style = MaterialTheme.typography.headlineLarge, modifier = Modifier.fillMaxWidth())
            Text(stringResource(R.string.eventDetails_timeRange, event.from.format(), event.to.format()))
        }
        event.absoluteImageUrl?.let { imageUrl ->
            Card(
                shape = MaterialTheme.shapes.small,
                modifier = Modifier
                    .weight(2.0f)
                    .aspectRatio(1.0f, false)
                    .padding(end = 16.dp),
            ) {
                GlideImage(
                    model = imageUrl,
                    modifier = Modifier,
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight,
                )
            }
        }
    }

    Spacer(Modifier.height(8.dp))

    Box(Modifier.padding(horizontal = 16.dp)) {
        Text(event.shortDescription, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Left)
    }

    // Note the placement of spacers in the conditions below.
    if (event.place != null) {
        Spacer(Modifier.height(8.dp))
        LinkWithIcon(
            modifier = Modifier.fillMaxWidth(),
            icon = {
                Icon(
                    painterResource(R.drawable.round_place_24),
                    contentDescription = stringResource(R.string.eventDetails_location)
                )
            },
            enabled = event.placeUrl != null,
            onClick = { context.openLink(event.placeUrl!!) },
            onClickLabel = stringResource(R.string.eventDetails_showLocation),
        ) {
            Text(event.place)
        }
    }
    Spacer(Modifier.height(8.dp))
    if (event.url != null) {
        LinkWithIcon(
            modifier = Modifier.fillMaxWidth(),
            icon = { Icon(painterResource(R.drawable.round_link_24), contentDescription = null) },
            enabled = true,
            onClick = { context.openLink(event.url) },
            onClickLabel = stringResource(R.string.eventDetails_showWebsite),
        ) {
            Text(stringResource(R.string.eventDetails_website))
        }
        Spacer(Modifier.height(8.dp))
    }

    if (event.fullDescription != null) {
        Divider()
        Spacer(Modifier.height(8.dp))
        Box(Modifier.padding(horizontal = 16.dp)) {
            Text(
                event.fullDescription,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Left
            )
        }
    }
}

@Composable
fun LinkWithIcon(
    icon: @Composable () -> Unit,
    enabled: Boolean,
    onClick: () -> Unit,
    onClickLabel: String,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) {
    val contentColor = if (enabled) MaterialTheme.colorScheme.secondary else LocalContentColor.current

    CompositionLocalProvider(LocalContentColor provides contentColor) {
        Row(
            modifier
                .fillMaxWidth()
                .clickable(
                    enabled = enabled,
                    onClick = onClick,
                    onClickLabel = onClickLabel,
                )
                .padding(horizontal = 16.dp, vertical = 8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            icon()
            Spacer(Modifier.width(8.dp))
            content()
        }
    }
}