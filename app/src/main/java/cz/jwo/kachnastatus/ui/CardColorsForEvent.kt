/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui

import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.Event
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.model.dataobjects.StateType

@Composable
internal fun cardColorsForEvent(event: EventOrState) = CardDefaults.cardColors(
    containerColor = event.getAssociatedColor(),
    contentColor = contentColorFor(event.getAssociatedColor()),
    disabledContainerColor = event.getAssociatedColor(),
    disabledContentColor = contentColorFor(event.getAssociatedColor()),
)

@Composable
internal fun EventOrState.getAssociatedColor(): Color =
    when (this) {
        is Event -> MaterialTheme.colorScheme.secondaryContainer
        is ClubState -> when (type) {
            StateType.OpenBar -> MaterialTheme.colorScheme.primaryContainer
            StateType.OpenChillzone -> MaterialTheme.colorScheme.tertiaryContainer
            StateType.Private -> MaterialTheme.colorScheme.secondaryContainer
            StateType.Closed -> MaterialTheme.colorScheme.secondaryContainer
        }
    }