/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package cz.jwo.kachnastatus.ui.pages.currentoffer

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow.Companion.Ellipsis
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.times
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.CurrentOffer
import cz.jwo.kachnastatus.model.dataobjects.OfferedItem
import cz.jwo.kachnastatus.ui.components.async.AsyncLoadingScreen
import cz.jwo.kachnastatus.ui.components.async.AsynchronouslyLoaded
import cz.jwo.kachnastatus.ui.getModel
import cz.jwo.kachnastatus.ui.getNavController

// TODO Caching

@Composable
fun CurrentOfferPage() {
    val navController = getNavController()

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(R.string.currentOfferPage_title)) },
                scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior(),
                navigationIcon = {
                    IconButton(
                        onClick = { navController.navigateUp() }
                    ) {
                        Icon(
                            painterResource(R.drawable.round_arrow_back_24),
                            contentDescription = stringResource(R.string.action_back)
                        )
                    }
                },
            )
        }
    ) { paddingValues ->
        val model = getModel()
        AsynchronouslyLoaded({ model.getCurrentOffer() }) { currentOfferOrError ->
            AsyncLoadingScreen(currentOfferOrError,
                loadingContent = {
                    Box(Modifier.padding(paddingValues)) {
                        Text("Loading…")
                    }
                }) { currentOfferFlow ->
                CurrentOfferPageContent(paddingValues, currentOfferFlow)
            }
        }
    }
}

@Composable
fun CurrentOfferPageContent(paddingValues: PaddingValues, currentOfferFlow: ModelValueFlow<CurrentOffer>) {
    val currentOffer = currentOfferFlow.collectAsState().value.value

    var selectedLabel by rememberSaveable { mutableStateOf<String?>(null) }

    LazyVerticalGrid(
        contentPadding = paddingValues,
        columns = GridCells.Adaptive(128.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        // Filtering.
        item(span = { GridItemSpan(maxCurrentLineSpan) }) {
            OfferItemsFilterChips(
                labels = currentOffer.labels,
                selectedLabel = selectedLabel,
                selectLabel = {
                    selectedLabel = it
                }
            )
        }

        val itemsToShow = selectedLabel?.let { currentOffer.getItemsByLabel(it) }
            ?: currentOffer.allItems

        // The items.
        items(itemsToShow, ::offerItemKey) { offeredItem ->
            Box(Modifier.padding(4.dp)) {
                CurrentOfferItem(offeredItem = offeredItem)
            }
        }
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun OfferItemsFilterChips(
    labels: Iterable<String>,
    selectedLabel: String?,
    selectLabel: (String?) -> Unit,
) {
    fun itemSelected(label: String?) = label == selectedLabel

    FlowRow(Modifier.padding(horizontal = 16.dp)) {
        (listOf<String?>(null) + labels).forEach { label ->
            val selected = itemSelected(label)

            Box(Modifier.padding(end = 8.dp)) {
                FilterChip(
                    selected = selected,
                    onClick = { selectLabel(label) },
                    label = { Text(label ?: stringResource(R.string.currentOfferPage_showAll)) }
                )
            }
        }
    }
}

private fun offerItemKey(item: OfferedItem): Any =
    item.name.hashCode() xor item.labels.hashCode() shl 8

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun CurrentOfferItem(offeredItem: OfferedItem, modifier: Modifier = Modifier) {
    val titleStyle = MaterialTheme.typography.titleMedium.copy(textAlign = TextAlign.Center)

    Card(modifier) {
        Card(
            Modifier
                .padding(8.dp)
                .aspectRatio(1.0f, matchHeightConstraintsFirst = false)
        ) {
            // TODO Use Glide support for LazyListState.
            GlideImage(
                offeredItem.imageUrl,
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
            )
        }
        Column(Modifier.padding(8.dp, 0.dp), horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                offeredItem.name,
                style = titleStyle,
                overflow = Ellipsis,
                modifier = Modifier.fillMaxWidth()
                    .height(with(LocalDensity.current) { 2 * titleStyle.lineHeight.toDp() }),
            )
            Text(stringResource(R.string.currentOfferPage_itemPrice, offeredItem.price.toString()))
            Spacer(Modifier.height(8.dp))
        }
    }
}