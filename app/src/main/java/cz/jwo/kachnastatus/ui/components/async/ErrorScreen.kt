/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.components.async

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.R

/**
 * Shows brief exception description and stack trace.
 */
@Composable
fun ErrorScreen(error: Exception, modifier: Modifier = Modifier) {
    val clipboardManager = LocalClipboardManager.current
    val context = LocalContext.current

    val errorHeader = buildAnnotatedString {
        withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
            appendLine(stringResource(R.string.errorScreen_title))
            append(" ".repeat(8))
            append(error.localizedMessage ?: error.message ?: error::class.java.name)
        }
    }
    val stacktrace = AnnotatedString(error.stackTraceToString())

    fun copyTrace() {
        clipboardManager.setText(buildAnnotatedString {
            append(errorHeader)
            appendLine()
            append(stacktrace)
        })
        Toast.makeText(
            context,
            context.resources.getText(R.string.toast_error_message_copied),
            Toast.LENGTH_SHORT
        ).show()
    }

    SelectionContainer(modifier = Modifier.padding(8.dp)) {
        Column(modifier) {
            Row {
                Text(buildAnnotatedString {
                    append(errorHeader)
                    appendLine()
                }, modifier = Modifier.weight(1f))

                IconButton(::copyTrace) {
                    Icon(
                        painterResource(R.drawable.round_copy_all_24),
                        contentDescription = stringResource(R.string.action_copy_error)
                    )
                }
            }
            Text(stacktrace)
        }
    }
}