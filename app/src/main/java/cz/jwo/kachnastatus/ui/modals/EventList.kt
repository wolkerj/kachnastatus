/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.modals

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.nav.ModalWindowContent
import cz.jwo.kachnastatus.ui.components.async.AsyncLoadingScreen
import cz.jwo.kachnastatus.ui.components.async.AsynchronouslyLoaded
import cz.jwo.kachnastatus.ui.components.calendar.CalendarEntryCard
import cz.jwo.kachnastatus.ui.getModel

@Composable
fun EventListModal(
    eventIds: List<ModalWindowContent.EventList.EventOrClubStateId>,
    modifier: Modifier,
) {
    val model = getModel()

    Text(
        stringResource(R.string.eventListModal_title),
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .padding(bottom = 16.dp)
    )

    AsynchronouslyLoaded({
        eventIds.map {
            when (it) {
                is ModalWindowContent.EventList.EventOrClubStateId.EventId -> model.getEvent(it.id)
                is ModalWindowContent.EventList.EventOrClubStateId.ClubStateId -> model.getClubState(it.id)
            }
        }
    }) { loadResult ->
        AsyncLoadingScreen(
            loadResult,
            errorScreenModifier = modifier,
            loadingContent = { Text("Loading…") }) { events ->
            Column {
                events.forEach { eventFlow ->
                    val event = eventFlow.collectAsState().value.value

                    CalendarEntryCard(event)
                    Spacer(Modifier.height(8.dp))
                }

                Spacer(Modifier.height(16.dp))
            }
        }
    }
}