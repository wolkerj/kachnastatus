/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.main

import cz.jwo.kachnastatus.model.Model
import cz.jwo.kachnastatus.model.ModelValueFlow
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.EventList
import cz.jwo.kachnastatus.model.dataobjects.PrestigeInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll

internal class MainPageData(
    val clubState: ModelValueFlow<ClubState>,
    val prestige: Deferred<Prestige>,
    val currentEvents: ModelValueFlow<EventList>,
) {
    class Prestige(
        val prestigeToday: ModelValueFlow<PrestigeInfo>,
        val prestigeSemester: ModelValueFlow<PrestigeInfo>,
    )
}

internal suspend fun Model.getMainPageData(coroutineScope: CoroutineScope): MainPageData {
    // We do not need the prestige and other values (expect the current state) to show the page, defer its loading.
    val prestige = coroutineScope.async {
        listOf(
            coroutineScope.async { getPrestigeLeaderboardToday() },
            coroutineScope.async { getPrestigeLeaderboardSemester() },
        ).awaitAll()
            .let { (today, semester) ->
                MainPageData.Prestige(today, semester)
            }
    }
    val (clubState, currentEvents) = listOf(
        coroutineScope.async { getCurrentClubState() },
        coroutineScope.async { getCurrentEvents() },
    ).awaitAll()
    @Suppress("UNCHECKED_CAST")
    return MainPageData(
        clubState as ModelValueFlow<ClubState>,
        prestige,
        currentEvents as ModelValueFlow<EventList>
    )
}