/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package cz.jwo.kachnastatus.ui.pages.main

import android.icu.text.DateFormat.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.*
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.dataobjects.StateType.*
import cz.jwo.kachnastatus.ui.components.async.AsyncLoadingScreen
import cz.jwo.kachnastatus.ui.components.async.AsynchronouslyLoaded
import cz.jwo.kachnastatus.ui.getModel
import kotlinx.coroutines.*
import java.util.*

@Composable
fun MainPage() {
    val scrollState = rememberScrollState()
    Box(Modifier.verticalScroll(scrollState)) {
        MainPageContent()
    }

    /** Status bar (or whatever the top inset is) scrim opacity. Its opacity increases as we scroll down. */
    val scrimAlpha = maxOf(
        0.0f, minOf(
            1.0f,
            LocalDensity.current.let { density ->
                density.run {
                    scrollState.value.toFloat() / (WindowInsets.safeDrawing.getTop(density) + 32.dp.toPx())
                }
            }
        )
    )

    /** Size of shadow behind the status bar scrim. */
    val scrimShadowSize = 4.dp

    // The scrim.
    Column(
        Modifier
            .fillMaxWidth()
            .alpha(scrimAlpha)
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .height(LocalDensity.current.run { WindowInsets.safeDrawing.getTop(this).toDp() })
                .background(Color.White)
        )
        Box(
            Modifier
                .fillMaxWidth()
                .height(scrimShadowSize)
                .background(
                    ShaderBrush(
                        LinearGradientShader(
                            Offset.Zero, Offset(0.0f, LocalDensity.current.run { scrimShadowSize.toPx() }),
                            listOf(DefaultShadowColor.copy(alpha = .2f), Color.Transparent)
                        )
                    )
                )
        )
    }
}

@Composable
private fun MainPageContent() {
    val model = getModel()
    val apiCoroutineScope = CoroutineScope(Dispatchers.IO)

    Column(modifier = Modifier.fillMaxWidth()) {
        val windowInsets = WindowInsets.safeDrawing.only(WindowInsetsSides.Top + WindowInsetsSides.Horizontal)
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth(),
        ) {
            MainPageTopBar(
                modifier = Modifier.fillMaxWidth(),
                windowInsets = windowInsets
            )

            Spacer(Modifier.height(16.dp))

            AsynchronouslyLoaded(
                load = {
                    model.getMainPageData(apiCoroutineScope)
                }
            ) { mainPageDataOrError ->
                Column(
                    Modifier
                        .fillMaxWidth()
                        .windowInsetsPadding(windowInsets.only(WindowInsetsSides.Horizontal)),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    AsyncLoadingScreen(mainPageDataOrError, loadingContent = {
                        Text("Loading…")
                    }
                    ) { mainPageData ->
                        StatusBlock(mainPageData.clubState, mainPageData.currentEvents)

                        EventsBlock(mainPageData.currentEvents)

                        AsynchronouslyLoaded({ mainPageData.prestige.await() }) { prestigeOrError ->
                            AsyncLoadingScreen(prestigeOrError, loadingContent = {
                                Text("Loading…")
                            }) { prestige ->
                                PrestigeBlock(prestige)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun MainPageTopBar(
    modifier: Modifier,
    windowInsets: WindowInsets,
) {
    Surface(
        modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onPrimaryContainer,
    ) {
        Row(
            Modifier.Companion
                .padding(vertical = 12.dp)
                .windowInsetsPadding(windowInsets.only(WindowInsetsSides.Top))
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Image(painterResource(R.drawable.kachna_bez_peny), contentDescription = null)
            Spacer(Modifier.width(8.dp))
            Column(horizontalAlignment = Alignment.Start) {
                Text(buildAnnotatedString {
                    appendLine("Studentský klub")
                    withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                        append("U Kachničky")
                    }
                })
            }
        }
    }
}
