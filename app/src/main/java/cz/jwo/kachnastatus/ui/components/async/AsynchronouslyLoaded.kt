/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.components.async

import androidx.compose.runtime.*

/**
 * Wraps asynchronously-loaded part of the UI.
 *
 * Executes the suspend function provided in [load] and uses the [content]. State of data loading
 * is provided to the [content].
 *
 * When the [key] changes, the data is loaded again. While the data is being loaded, the old
 * [content] is used.
 *
 * When an exception is thrown from [load], error state is passed to the [content].
 *
 * @param key When this expression changes value, the load is performed again.
 *
 * @see AsynchronousLoadResult
 * @see AsyncLoadingScreen
 */
@Composable
fun <T, K> AsynchronouslyLoaded(
    load: suspend (K) -> T,
    key: K,
    content: @Composable (AsynchronousLoadResult<T>) -> Unit,
) {
    var state by remember { mutableStateOf(AsynchronousLoadResult<T>()) }
    LaunchedEffect(key) {
        state = try {
            AsynchronousLoadResult(value = load(key))
        } catch (exception: java.lang.Exception) {
            exception.printStackTrace()
            AsynchronousLoadResult(error = exception)
        }
    }
    content(state)
}

/**
 * Wraps asynchronously-loaded part of the UI.
 *
 * Executes the suspend function provided in [load] and uses the [content]. State of data loading
 * is provided to the [content].
 *
 * When an exception is thrown from [load], error state is passed to the [content].
 *
 * @see AsynchronousLoadResult
 * @see AsyncLoadingScreen
 */
@Composable
fun <T> AsynchronouslyLoaded(
    load: suspend () -> T,
    content: @Composable (AsynchronousLoadResult<T>) -> Unit,
) {
    AsynchronouslyLoaded(
        load = { load() },
        key = 0,
        content = content,
    )
}

/**
 * Value passed to content of [AsynchronouslyLoaded].
 */
class AsynchronousLoadResult<T> private constructor(val value: T? = null, val error: Exception? = null) {
    constructor(value: T? = null) : this(value, null)
    constructor(error: java.lang.Exception) : this(null, error)

    /** Whether the load ended with an error. */
    val isError get() = error != null

    /** Whether the load ended with a success. */
    val isDone get() = value != null
}