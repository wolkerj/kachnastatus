/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.ui.pages.calendar

import android.os.Build
import android.view.View
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.DividerDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.Event
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.nav.ModalWindowContent
import cz.jwo.kachnastatus.ui.LocalModalWindowManager
import cz.jwo.kachnastatus.ui.components.calendar.SmallEventItem
import cz.jwo.kachnastatus.util.*
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.util.*

private val minRowHeight = 128.dp

/**
 * View of the calendar that shows a week per row and events only by their name.
 */
@Composable
internal fun WeeksCalendar(
    startDate: Date,
    endDate: Date,
    eventsFlow: StateFlow<ModelResponse<List<EventOrState>>>,
    padding: PaddingValues = PaddingValues(0.dp),
) {
    val events = eventsFlow.collectAsState().value.value

    // Adjust the start and end date.
    val firstWeekStart = startDate.weekStart
    val lastWeekEnd = endDate.nextWeekStart

    /** List of all days to show, grouped by week. */
    val weekDays = generateSequence(firstWeekStart) { it.midnightAfter }
        .map { DateRange(it, it.midnightAfter) }
        .chunked(7)
        .takeWhile { it.last().to.time <= lastWeekEnd.time }

    /** List of all days with their events, grouped by week. */
    val weekItems = weekDays
        .map { daysOfWeek ->
            daysOfWeek.map { day ->
                DayEvents(day, events.filter { DateRange(it.from, it.to ?: it.from) intersects day }.toList())
            }
        }
        .toList()

    Box(Modifier.padding(top = padding.calculateTopPadding())) {
        val dir = when (LocalConfiguration.current.layoutDirection) {
            View.LAYOUT_DIRECTION_LTR -> LayoutDirection.Ltr
            View.LAYOUT_DIRECTION_RTL -> LayoutDirection.Rtl
            else -> throw AssertionError()
        }

        // We do not need to scroll the vertical separators. We simply draw them in background
        // behind the scrollable list.
        WeekCalendarColumnSeparators(Modifier.fillMaxSize())

        // The scrollable list itself (a row per week).
        LazyColumn(
            Modifier.fillMaxWidth(),
            contentPadding = PaddingValues(
                start = padding.calculateStartPadding(dir),
                end = padding.calculateEndPadding(dir),
                top = 0.dp,
                bottom = padding.calculateBottomPadding(),
            )
        ) {
            items(weekItems, key = { it.first().day }) { weekDays ->
                WeekEvents(weekDays)
                Divider()
            }
        }
    }
}

@Composable
private fun WeekCalendarColumnSeparators(modifier: Modifier = Modifier) {
    Row(modifier) {
        (0..6).forEach { index ->
            Spacer(Modifier.weight(1.0f))
            if (index != 6) {
                Box(
                    Modifier
                        .width(DividerDefaults.Thickness)
                        .fillMaxHeight()
                        .background(DividerDefaults.color)
                )
            }
        }
    }
}

@Composable
private fun WeekEvents(weekDays: List<DayEvents>) {
    val composeScope = rememberCoroutineScope()
    val modalWindowManager = LocalModalWindowManager.current

    Row(horizontalArrangement = Arrangement.Start, modifier = Modifier.height(IntrinsicSize.Max)) {
        weekDays.forEach { (dayRange, events) ->
            WeekDayItem(
                dayRange = dayRange,
                events = events,
                modifier = Modifier
                    .defaultMinSize(minHeight = minRowHeight)
                    .weight(1.0f)
                    .clickable(enabled = events.isNotEmpty()) {
                        composeScope.launch {
                            modalWindowManager.showModal(
                                ModalWindowContent.EventList(
                                    events.map {
                                        when (it) {
                                            is ClubState -> ModalWindowContent.EventList.EventOrClubStateId.ClubStateId(
                                                it.id
                                            )

                                            is Event -> ModalWindowContent.EventList.EventOrClubStateId.EventId(
                                                it.id
                                            )
                                        }
                                    })
                            )
                        }
                    }
            )
        }
    }
}

/**
 * Single cell of the calendar table.
 */
@Composable
private fun WeekDayItem(dayRange: DateRange, events: List<EventOrState>, modifier: Modifier = Modifier) {
    val locale =
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            LocalConfiguration.current.locales[0]
        } else {
            LocalConfiguration.current.locale
        }
    val dayFormat = java.text.SimpleDateFormat(
        stringResource(R.string.calendarPage_weekly_dayLabelFormat),
        locale
    )

    Column(modifier.padding(2.dp, 2.dp)) {
        Text(dayFormat.format(dayRange.from))
        Spacer(Modifier.height(2.dp))

        events.forEach { eventOrState ->
            SmallEventItem(eventOrState)
            Spacer(Modifier.height(4.dp))
        }

        // Stretch to all remaining space.
        Spacer(Modifier.weight(1.0f))
    }
}
