/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package cz.jwo.kachnastatus.ui.pages.calendar

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.model.ModelResponse
import cz.jwo.kachnastatus.model.dataobjects.EventOrState
import cz.jwo.kachnastatus.model.getEventsAndStatesBetween
import cz.jwo.kachnastatus.settings.AppSettingsStateHolder.*
import cz.jwo.kachnastatus.settings.CalendarStyle
import cz.jwo.kachnastatus.ui.*
import cz.jwo.kachnastatus.ui.components.async.AsyncLoadingScreen
import cz.jwo.kachnastatus.ui.components.async.AsynchronouslyLoaded
import cz.jwo.kachnastatus.util.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*

internal const val TAG = "CalendarPage"

@Composable
fun CalendarPage() {
    val appSettings = LocalAppSettings.current
    val navController = getNavController()

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(R.string.calendarPage_title)) },
                navigationIcon = {
                    IconButton(
                        onClick = { navController.navigateUp() }
                    ) {
                        Icon(
                            painterResource(R.drawable.round_arrow_back_24),
                            contentDescription = stringResource(R.string.action_back)
                        )
                    }
                },
                actions = {
                    Crossfade(appSettings.calendarStyle) {
                        when (it) {
                            CalendarStyle.SeparateEvents -> IconButton(
                                onClick = { appSettings.calendarStyle = CalendarStyle.Weeks },
                            ) {
                                Icon(
                                    painterResource(R.drawable.round_calendar_view_month_24),
                                    contentDescription = stringResource(R.string.calendarPage_action_showWeekly)
                                )
                            }

                            CalendarStyle.Weeks -> IconButton(
                                onClick = { appSettings.calendarStyle = CalendarStyle.SeparateEvents },
                            ) {
                                Icon(
                                    painterResource(R.drawable.round_calendar_view_day_24),
                                    contentDescription = stringResource(R.string.calendarPage_action_showDaily)
                                )
                            }
                        }
                    }
                }
            )
        }
    ) { paddingValues ->
        CalendarPageContent(paddingValues)
    }
}

@Composable
private fun CalendarPageContent(padding: PaddingValues) {
    val model = getModel()
    val coroutineScope = rememberCoroutineScope()

    val startDate = Date()
    val endDate = GregorianCalendar().apply {
        add(Calendar.MONTH, 4)
    }.time
    val statesEndDate = GregorianCalendar().apply {
        add(Calendar.MONTH, 1)
    }.time

    AsynchronouslyLoaded({
        model.getEventsAndStatesBetween(startDate, endDate, statesEndDate, coroutineScope)
    }) {
        AsyncLoadingScreen(loadResult = it,
            errorScreenModifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .padding(padding),
            loadingContent = {
                Box(Modifier.padding(padding)) { Text("Loading…") }
            }) { events: StateFlow<ModelResponse<List<EventOrState>>> ->
            Crossfade(LocalAppSettings.current.calendarStyle) { calendarStyle ->
                when (calendarStyle) {
                    CalendarStyle.Weeks -> WeeksCalendar(startDate, endDate, events, padding = padding)
                    CalendarStyle.SeparateEvents -> SeparateEventsCalendar(events, padding = padding)
                }
            }
        }
    }
}