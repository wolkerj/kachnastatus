/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.activities

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.core.view.WindowCompat
import cz.jwo.kachnastatus.api.DefaultApiClient
import cz.jwo.kachnastatus.model.cache.impl.CacheCombinator
import cz.jwo.kachnastatus.model.cache.impl.InMemoryCache
import cz.jwo.kachnastatus.model.cache.impl.logAccesses
import cz.jwo.kachnastatus.model.dataobjects.ClubState
import cz.jwo.kachnastatus.model.dataobjects.PrestigeInfo
import cz.jwo.kachnastatus.model.impl.CurrentClubStateCacheHook
import cz.jwo.kachnastatus.model.impl.DirectApiAccessModel
import cz.jwo.kachnastatus.model.impl.EventAndClubStateCacheHook
import cz.jwo.kachnastatus.model.impl.PrestigeLeaderboardCacheHook
import cz.jwo.kachnastatus.model.utils.DummyModel
import cz.jwo.kachnastatus.model.utils.MutableModelWrapper
import cz.jwo.kachnastatus.model.utils.mock.FixedCurrentClubStateProvider
import cz.jwo.kachnastatus.model.utils.toCompositeModel
import cz.jwo.kachnastatus.settings.AppSettings
import cz.jwo.kachnastatus.settings.AppSettingsStateHolder
import cz.jwo.kachnastatus.settings.AppSettingsStorage
import cz.jwo.kachnastatus.settings.ApplicationTheme
import cz.jwo.kachnastatus.ui.AppUi
import cz.jwo.kachnastatus.ui.LocalAppSettings
import cz.jwo.kachnastatus.ui.LocalModalWindowManager
import cz.jwo.kachnastatus.ui.ModelProvider
import cz.jwo.kachnastatus.ui.theme.KachnaStatusTheme
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.stateIn

private const val TAG = "MainActivity"

class MainActivity : ComponentActivity() {
    val model = MutableModelWrapper(DummyModel)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            loadAppSettings()
            initModel()

            // Every time when the settings change, store them.
            val appSettings = LocalAppSettings.current
            LaunchedEffect(true) {
                // This “trick” stores the settings when launched and monitors the settings
                // (stored in MutableStates in AppSettingsStateHolder) for changes. The flow
                // produces new value every time when any of the values changes. When a new
                // value is produced, the settings are saved. The values produced are ignored.
                snapshotFlow {
                    Log.d(TAG, "Saving application settins.")
                    saveAppSettings(appSettings)
                }.collect()
            }

            val modalWindowManager = LocalModalWindowManager.current

            KachnaStatusTheme(
                darkTheme = when (LocalAppSettings.current.theme) {
                    ApplicationTheme.Light -> false
                    ApplicationTheme.Default -> false
                    ApplicationTheme.Dark -> true
                    ApplicationTheme.FollowSystem -> isSystemInDarkTheme()
                },
                bottomSheetShown = modalWindowManager.bottomSheetShown,
            ) {
                ModelProvider(model) {
                    Box(Modifier.fillMaxSize().background(MaterialTheme.colorScheme.background))
                    AppUi(modalWindowManager)
                }
            }
        }
    }

    @Composable
    private fun loadAppSettings() {
        LocalAppSettings.current.loadSettingsFrom(AppSettingsStorage(this).getSettings())
    }

    private fun saveAppSettings(appSettings: AppSettings) {
        AppSettingsStorage(this).saveSettings(appSettings)
    }

    private fun saveAppSettings(appSettingsStateHolder: AppSettingsStateHolder) {
        saveAppSettings(appSettingsStateHolder.toAppSettings())
    }

    @Composable
    private fun initModel() {
        val appSettings = LocalAppSettings.current

        val staticModel = DirectApiAccessModel(DefaultApiClient)

        // Update the model from the application preferences.
        LaunchedEffect(true) {
            // TODO Fix (de)serialization issues with the disk cache and set it up.
            //val cacheDatabase = DiskCache.openDatabase(context = this@MainActivity)

            val currentStateCache = InMemoryCache<ClubState>(10_000)
                .logAccesses("cache:CurrentState")
            val eventCache = CacheCombinator(
                InMemoryCache<EventAndClubStateCacheHook.CacheEntry>(3600_000)
                    .logAccesses("cache:Event-mem"),
                //DiskCache<EventCacheHook.CacheEntry>(cacheDatabase, kind = "event", expirationMillis = 3600_000)
                //.logAccesses("cache:Event-disk"),
            ).logAccesses("cache:Event")
            val prestigeLeaderboardCache = InMemoryCache<PrestigeInfo>(60_000)
                .logAccesses("cache:Prestige")

            snapshotFlow {
                val newModel = MutableModelWrapper(staticModel.toCompositeModel())

                appSettings.developer.mockClubState?.let { mockState ->
                    newModel.hook {
                        currentClubStateProvider = FixedCurrentClubStateProvider(mockState)
                    }
                }

                newModel.hook(CurrentClubStateCacheHook(currentStateCache))
                newModel.hook(EventAndClubStateCacheHook(eventCache))
                newModel.hook(PrestigeLeaderboardCacheHook(prestigeLeaderboardCache))

                newModel
            }.stateIn(this).collect { model ->
                Log.d(TAG, "Changing the data model.")
                this@MainActivity.model.innerModel = model
                Log.d(TAG, "New model: ${this@MainActivity.model.innerModel}")
            }
        }
    }
}
