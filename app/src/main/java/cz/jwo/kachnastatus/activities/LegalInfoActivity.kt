/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.activities

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import cz.jwo.kachnastatus.R
import cz.jwo.kachnastatus.ui.util.openLink

private const val TAG: String = "LegalInfoActivity"

class LegalInfoActivity : AppCompatActivity() {
    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_license_info)

        val displayUp = intent.extras?.getBoolean(CAN_GO_UP, false) ?: false
        supportActionBar!!.apply {
            setDisplayHomeAsUpEnabled(displayUp)
            setHomeButtonEnabled(displayUp)
        }

        webView = findViewById(R.id.webView)
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest): Boolean {
                Log.d(TAG, "Navigating to ${request.url}")
                if (request.url.host == "asset") {
                    openAsset(request.url.path!!.trimStart('/'))
                } else {
                    openLink(request.url.toString())
                }
                return true
            }
        }
        webView.settings.apply {
            blockNetworkLoads = true
            useWideViewPort = true
            builtInZoomControls = true
        }

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState)
        } else {
            openAsset("legal.html")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    override fun onNavigateUp(): Boolean {
        if (!goBack()) {
            Log.d(TAG, "Cannot go back, starting the new Activity.)")
            finish()
        }
        return true
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean =
        goBack() || super.onKeyDown(keyCode, event)

    private fun goBack(): Boolean =
        if (webView.canGoBack()) {
            webView.goBack()
            true
        } else false

    private fun openAsset(assetFile: String) {
        val html = assets.open(assetFile).bufferedReader(Charsets.UTF_8).readText()
        val mimeType = if (assetFile.endsWith(".html")) "text/html" else "text/plain"
        webView.loadDataWithBaseURL("http://asset/$assetFile", html, mimeType, "utf-8", assetFile)
    }

    companion object {
        val CAN_GO_UP = Companion::class.java.canonicalName!! + ".CAN_GO_UP"
    }
}
