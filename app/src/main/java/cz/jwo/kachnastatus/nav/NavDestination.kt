/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.nav

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import cz.jwo.kachnastatus.R

/**
 * Specifies a specific page in the application.
 */
sealed class NavDestination private constructor(
    /**
     * Text that uniquely identifies the location.
     */
    val route: String,
    /**
     * String resource containing localized name of the location.
     */
    @StringRes open val name: Int,
    /**
     * Drawable resource containing icon that is used to represent the location.
     */
    @DrawableRes open val icon: Int,
) {
    object Main : NavDestination(
        route = "main",
        name = R.string.navDestination_main_name,
        icon = R.drawable.round_home_24,
    )

    object Calendar : NavDestination(
        route = "calendar",
        name = R.string.navDestination_calendar_name,
        icon = R.drawable.round_calendar_month_24,
    )

    object CurrentOffer : NavDestination(
        route = "currentOffer",
        name = R.string.navDestination_currentOffer_name,
        icon = R.drawable.round_fastfood_24,
    )

    object Settings : NavDestination(
        route = "settings",
        name = R.string.navDestination_settings_name,
        icon = R.drawable.round_settings_24,
    )
}