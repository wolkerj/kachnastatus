/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.nav

import android.util.Log
import androidx.compose.material.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SheetState
import androidx.compose.material3.SheetValue
import androidx.compose.runtime.*

private const val TAG = "ModalWindowManager"

/**
 * Manages state of currently shown modal windows.
 */
class ModalWindowManager {
    /**
     * Currently shown modal bottom sheet.
     */
    var currentBottomSheet by mutableStateOf<ModalWindowContent?>(null)

    /**
     * List of currently shown modal dialogs.
     *
     * The topmost dialog is the last.
     */
    val modalDialogs = mutableStateListOf<ModalWindowContent>()

    /**
     * State of the currently shown modal bottom sheet.
     */
    @OptIn(ExperimentalMaterial3Api::class)
    val sheetState = SheetState(skipCollapsed = true, initialValue = SheetValue.Hidden, confirmValueChange = {
        Log.d(TAG, "SheetState reports change to $it.")
        if (it == SheetValue.Hidden) {
            bottomSheetShown = false
        }
        true
    })

    /**
     * Whether the modal bottom sheet is shown.
     *
     * When this property is set to false, the sheet will animate to the closed state and only then
     * the sheet becomes closed.
     */
    var bottomSheetShown by mutableStateOf(false)

    /**
     * Whether the modal bottom sheet is visible or is going to open.
     */
    @OptIn(ExperimentalMaterial3Api::class)
    val bottomSheetActuallyShown get() = bottomSheetShown || sheetState.isVisible

    /**
     * Requests the modal bottom sheet to be shown and waits until it is fully shown.
     */
    @OptIn(ExperimentalMaterial3Api::class)
    private suspend fun showBottomSheet(bottomSheet: ModalWindowContent) {
        Log.d(TAG, "Showing modal sheet.")
        bottomSheetShown = true
        currentBottomSheet = bottomSheet
        sheetState.show()
        Log.d(TAG, "Modal sheet shown.")
    }

    private fun showModalDialog(content: ModalWindowContent) {
        modalDialogs.add(content)
    }

    suspend fun showModal(content: ModalWindowContent) {
        if (!bottomSheetShown) {
            showBottomSheet(content)
        } else {
            showModalDialog(content)
        }
    }

    /**
     * Requests the modal bottom sheet to be closed and waits until it is fully closed.
     */
    @OptIn(ExperimentalMaterial3Api::class)
    suspend fun closeBottomSheet() {
        Log.d(TAG, "Hiding modal sheet.")
        sheetState.hide()
        bottomSheetShown = false
        currentBottomSheet = null
        Log.d(TAG, "Modal sheet hidden.")
    }

    /**
     * Closes the topmost modal dialog.
     */
    fun closeModalDialog() {
        modalDialogs.removeLast()
    }
}