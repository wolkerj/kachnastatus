/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.nav

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cz.jwo.kachnastatus.ui.modals.EventDetailsModal
import cz.jwo.kachnastatus.ui.modals.EventListModal

/**
 * Provides information about what should be shown in the modal bottom sheet.
 */
sealed class ModalWindowContent {
    /**
     * Composes the modal bottom sheet contents.
     */
    @Composable
    abstract fun Composable(modifier: Modifier)

    /**
     * [ModalWindowContent] that shows detail of some specified event.
     */
    data class EventDetails(val eventId: Int) : ModalWindowContent() {
        @Composable
        override fun Composable(modifier: Modifier) {
            EventDetailsModal(eventId, modifier)
        }
    }

    /**
     * [ModalWindowContent] that shows list of events.
     */
    data class EventList(var eventIds: List<EventOrClubStateId>) : ModalWindowContent() {
        @Composable
        override fun Composable(modifier: Modifier) {
            EventListModal(eventIds, modifier)
        }

        /**
         * ID of entry on the list.
         */
        sealed class EventOrClubStateId {
            class EventId(val id: Int) : EventOrClubStateId()
            class ClubStateId(val id: Int) : EventOrClubStateId()
        }
    }
}