/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.api

import android.util.Log
import cz.jwo.kachnastatus.model.dataobjects.*
import cz.jwo.kachnastatus.util.toIso8601Date
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.serialization.kotlinx.json.*
import java.util.*

private const val TAG = "API"

/**
 * Client to the Kachna Online API.
 *
 * This client internally uses Ktor CIO backend for accessing the API.
 */
class ApiClient(
    /**
     * Base URL of the API.
     *
     * Defaults to the production API.
     */
    private val baseUrl: String = "https://www.su.fit.vutbr.cz/kachna/api/",
) {

    /**
     * Ktor [HttpClient] that is used for accessing the API.
     */
    private val httpClient = HttpClient(CIO) {
        // Throw on API errors.
        expectSuccess = true

        //install(HttpRequestRetry) {
        //retryOnServerErrors(maxRetries=5)
        //retryOnException(maxRetries=15, retryOnTimeout = true)
        //exponentialDelay()
        //}

        // Set the default request config.
        install(DefaultRequest)
        defaultRequest {
            url(baseUrl)
        }
        BrowserUserAgent()

        // Request processing config.
        install(ContentNegotiation) {
            json()
        }
    }

    /**
     * Performs an API request to the given relative URL.
     *
     * This method should be used for all API calls.
     */
    private suspend fun doRequest(url: String): HttpResponse {
        Log.d(TAG, "Performing GET request to $url")
        val response = httpClient.request(url)
        Log.d(TAG, "Response:\n${response.bodyAsText()}")
        return response
    }

    /**
     * Get current state of the club.
     */
    suspend fun getCurrentState() = doRequest("states/current").body<ClubState>()

    /**
     * Get prestige leaderboard for this day.
     */
    suspend fun getPrestigeToday() = doRequest("club/leaderboard/today").body<PrestigeInfo>()

    /**
     * Get prestige leaderboard for this semester.
     */
    suspend fun getPrestigeSemester() = doRequest("club/leaderboard/semester").body<PrestigeInfo>()

    /**
     * Get list of events happening during the specified time interval.
     *
     * The server can restrict maximum range to fetch. Fetching approximately
     * one month of months should be safe.
     *
     * The duration of the events must lie completely in the range.
     */
    suspend fun getEvents(from: Date, to: Date) =
        doRequest("events?from=${from.toIso8601Date}&to=${to.toIso8601Date}").body<EventList>()
            .map(Event::normalize)

    /**
     * Returns a list of currently happening events.
     *
     * Note that multiple events and club states can be active at one moment.
     */
    suspend fun getCurrentEvents() =
        doRequest("events/current").body<EventList>()
            .map(Event::normalize)

    /**
     * Returns event information by its ID.
     */
    suspend fun getEvent(eventId: Int) =
        doRequest("events/$eventId?withLinkedStates=false").body<Event>()
            .normalize()

    /**
     * Returns list of club states happening in the specified time span.
     *
     * The notes from [getCurrentEvents] apply also here.
     */
    suspend fun getStates(from: Date, to: Date) =
        doRequest("states?from=${from.toIso8601Date}&to=${to.toIso8601Date}").body<ClubStateList>()

    /**
     * Returns club state information by its ID.
     */
    suspend fun getClubState(stateId: Int) =
        doRequest("states/$stateId")
            .body<ClubState>()

    /**
     * Returns information about the currently offered items.
     */
    suspend fun getCurrentOffer() =
        doRequest("club/offer").body<CurrentOffer>()
}

/**
 * The API client instance used for the API accesses.
 */
val DefaultApiClient = ApiClient()