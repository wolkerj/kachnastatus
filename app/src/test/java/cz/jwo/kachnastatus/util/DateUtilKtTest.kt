/*
 * KachnaStatus
 * Copyright (c) Jiří Wolker 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cz.jwo.kachnastatus.util

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

class DateUtilKtTest {
    @Test
    fun midnight() {
        val date = Date(1677557744567L)
        assertEquals(1677542400000L, date.midnight.time)
    }

    @Test
    fun midnightAfter() {
        val date = Date(1677557744567L)
        apply {
            println("Testing midnightAfter on non-midnight time.")
            assertEquals(1677542400000L, date.midnightAfter.time)
        }
        apply {
            println("Testing midnightAfter on a midnight.")
            assertEquals(1677628800000L, date.midnightAfter.midnightAfter.time)
        }
    }

    @Test
    fun midnightsBetween() {
        apply {
            println("Testing midnightsBetween() on two non-midnight times.")
            val startTime = 1677454144567L
            val endTime = 1678059594728L // approx one week later
            val start = Date(startTime)
            val end = Date(endTime)
            assertArrayEquals(generateSequence(start.midnightAfter.time) { it + 3600_000 * 24 }
                .map(::Date)
                .take(6).toList().toTypedArray(), midnightsBetween(start, end).toTypedArray())
        }
        apply {
            println("Testing midnightsBetween() on two midnights.")
            val startTime = 1677538800000L
            val endTime = 1678143600000L // exactly one week later
            val start = Date(startTime)
            val end = Date(endTime)
            assertArrayEquals(
                generateSequence(start.midnightAfter.time) { it + 3600_000 * 24 }
                    .map(::Date)
                    .take(6).toList().toTypedArray(),
                midnightsBetween(start, end).toTypedArray()
            )
        }
    }

    @Test
    fun between() {
        assertTrue(Date(1677538850000L).between(DateRange(Date(1677538800000L), Date(1677639800000L))))
        assertFalse(Date(1677438850000L).between(DateRange(Date(1677538800000L), Date(1677639800000L))))
        assertFalse(Date(1678638850000L).between(DateRange(Date(1677538800000L), Date(1677639800000L))))
    }

    @Test
    fun spansMultipleDays() {
        assertTrue(DateRange(Date(1677454144567L), Date(1678059594728L)).spansMultipleDays)
        assertFalse(DateRange(Date(1677454144567L), Date(1677454184567L)).spansMultipleDays)
    }
}