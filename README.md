KachnaStatus 🦆 (beta version)
================================================================================

This app provides easy-to-use interface to query status of the [Student club 
U Kačenky][kachna-online] at the [FIT BUT][fit].

The application shows the following information:

  * state of the club (closed, chillzone, open, event etc.),
  * current event in the club,
  * prestige leaderboard,
  * currently offered products,
  * calendar with upcoming events

**This app is unofficial,** I am developing it only for my personal purposes 
and friends. If you want to use an official application, use [the web 
interface (a.k.a. *Kachna Online*)](kachna-online).

Planned features
----------------

  * Week view of the event calendar
  * Notifications before changes of the club state
  * Alarm-like notification before selected club state changes
  * Persistent notification and Android widget with the current state
  * Android widget with the leaderboard
  * Android calendar integration
  * Integration with features that require log-in to the web app (mostly 
    things that are used only by [the Students Union][su])
  * … and of course many bugfixes

Known bugs
----------

  * The colors are bad.
  * The same applies to the dark mode, but even more intensively.
  * When API requests are being awaited, „Loading…“ is shown. Unlocalized.
  * The localization is not complete.
  * Some UI components look quite bar, for example the product cards on the 
    Current offer page.
  * Only calendar in form of event list is available. The week view shows 
    only an empty screen.
  * Most settings entries are not working.
  * When the display is too narrow, labels in the bottom menu get wrapped 
    and overflow their container.
  * Disk caching implementation is present in the source code tree, but not 
    working (and therefore not enabled at runtime).
  * Some scrollable pages with items (most notably the Current offer) have 
    severe issues with dropping frames.

And some meta-bugs:

  * Licensing information is missing (to be added in future commits)
  * The app is not available from F-Droid app repository (coming soon)
  * The app is not available from the Google Play Store (probably WONTFIX)
  * The app should link to the official web app etc.

[kachna-online]: https://su.fit.vut.cz/kachna
[su]: https://su.fit.vut.cz
[fit]: https://fit.vutbr.cz