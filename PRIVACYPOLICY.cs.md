Informace o ochraně osobních údajů
==================================

Aplikace KachnaStatus nijak nezaznamenává žádné osobní údaje.

Pro získání informací o stavu studentského klubu je použito
veřejné rozhraní dostupné z Internetu. Provozovatelem tohoto
rozhraní je Studentská unie FIT VUT v Brně. Vývojář aplikace
nemá žádnou možnost ovlivnit to, jak je nakládáno s informaceni
o síťovém spojení na straně provozovatele rozhraní – pro více
informací vizte https://www.su.fit.vutbr.cz/.
